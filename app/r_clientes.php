<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class r_clientes extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function r_ventas()
    {
        return $this->hasMany(r_venta::class);
    }
    public function suscripcions()
    {
        return $this->hasMany(suscripcion::class);
    }
}
