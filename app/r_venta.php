<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class r_venta extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function r_clientes()
    {
        return $this->belongsTo(r_clientes::class, 'r_cliente_id');
    }
    public function r_detalles()
    {
        return $this->hasMany(r_detalles::class);
    }
}
