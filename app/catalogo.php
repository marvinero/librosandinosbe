<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class catalogo extends Model
{
    //
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    public function libros()
    {
        return $this->hasMany(libro::class, 'catalogo_id');
    }
    
}
