<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class r_detalles extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
     
    public function numeros()
    {
        return $this->belongsTo(numero::class);
    } 
     public function revistas()
    {
        return $this->belongsTo(revista::class);
    }
    public function r_ventas()
    {
        return $this->belongsTo(r_venta::class);
    }
}
