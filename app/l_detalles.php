<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class l_detalles extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function libros()
    {
        return $this->belongsTo(libro::class);
    }
    public function l_ventas()
    {
        return $this->belongsTo(l_venta::class,'l_venta_id');
    }
}
