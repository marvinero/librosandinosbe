<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class suscripcion extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function revistas()
    {
        return $this->belongsTo(revista::class);
    }

    public function r_clientes()
    {
        return $this->belongsTo(r_clientes::class);
    }
}
