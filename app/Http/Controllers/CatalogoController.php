<?php

namespace App\Http\Controllers;

use App\catalogo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('catalogos')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        return response()->json($data, 200); 
    }

     public function catalogoLibro(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('catalogos')->where('estado', 'abierto')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $catalogo = catalogo::create([
            'categoria' => $data['categoria'],
            'tipo' => $data['tipo'],
            'registro' => $data['registro'],
            'estado' => $data['estado'],
            'nombre' => $data['nombre'],
            'periodo' => $data['periodo'],
            'gestion' => $data['gestion'],
            'file' => $data['file'],
            'pais' => $data['pais'],
            'send' => $data['send'],
            'text' => $data['text'],
            'pais' => $data['pais'],
            'user_id' => $data['user_id']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$catalogo->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $catalogo = catalogo::find($id);
        $request->validate([
            'nombre' => 'required|string|max:255'
        ]);
        $requestData = $request->all();

        //$catalogo->update($requestData);

        if($request->estado != $catalogo->estado){
            //$numlenght = strlen((string)(count($catalogo->libros)+1));
            $numlenght=1;
            $numval=1;
            $cod=$request->categoria;
            foreach ($catalogo->libros as $key => $value) {
                $numval=$key+1;
                $numlenght = strlen((string)($numval));
                $numero = '';
                $valCod='';
                if($numlenght == 1){

                    $numero=((string)"00$numval");
                }else{
                    if($numlenght == 2){
                        $numero=((string)"0$numval");

                    }else{
                        $numero=((string)$numval);

                    }          
                }
                if ($request->estado!='cerrado') {

                    $valCod = $cod."TMP$numero";

                }else{
                    $valCod = $cod.((string)$numero);
                }

                $value->cod=$valCod;
                $value->update();
                //$numlenght++;

            }
        }
        $catalogo->update($requestData);

        $mensaje = "Actualizado correctamente";

        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catalogo = catalogo::find($id);
        $catalogo->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
