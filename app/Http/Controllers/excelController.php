<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\l_detalles;
use Illuminate\Support\Facades\DB;

class excelController extends Controller
{
  private $excel;

   public function __construct(Excel $excel)
   {
       $this->excel = $excel;
   }

   public function exportLDetalleExcel($id)
   {
      $data = DB::table('l_detalles')->where('l_venta_id',$id)->orderBy('created_at', 'asc')->get();

      return $this->excel->download($data, 'lDetalles.xlsx');
   }

   public function exportViaMethodInjection(Excel $excel){

      Excel::import(new UsersImport,request()->file('file'));

      return back();
   }

}
