<?php

namespace App\Http\Controllers;

use App\l_clientes;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class LClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('l_clientes')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'empresa' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $l_cliente = l_clientes::create([
            'username' => $data['username'],
            'password' => $data['password'],
            'email' => $data['email'],
            'web' => $data['web'],
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'empresa' => $data['empresa'],
            'telefono' => $data['telefono'],
            'direccion' => $data['direccion'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje' => $mensaje, 'id' => $l_cliente->id], 200);
    }


    public function update(Request $request, $id)
    {
        $l_cliente = l_clientes::find($id);
        $request->validate([
            'empresa' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $l_cliente->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\l_clientes  $l_cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $l_cliente = l_clientes::find($id);
        $l_cliente->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }
}
