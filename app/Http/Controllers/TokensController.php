<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenBlacklistException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class TokensController extends Controller
{
    public function login(Request $request){

    	$credentials = $request->all();

    	$validator = Validator::make($credentials,[
    		'username' => 'required',
    		'password' => 'required'
    	]);

    	if($validator->fails()){
    		return response()->json([
    			'success' => false,
    			'message' =>'Error validacion',
    			'errors' => $validator->errors()
    		], 422);
    	}

    	$token = JWTAuth::attempt($credentials);

    	return response()->json( compact('token') );
    }

    public function refreshToken(){

    	$token = JWTAuth::getToken();
    	try{
    		$token = JWTAuth::refresh($token);
    		return response()->json([
    			'success' => true,
    			'token' => $token,  			
    		], 200);

    	}catch(TokenExpiredException $ex){

    		return response()->json([
    			'success' => false,
    			'message' =>'La sesion expiro',
    		], 422);
    	}
    	catch(TokenBlacklistException $ex){
    		return response()->json([
    			'success' => false,
    			'message' =>'Necesitas Iniciar sesion otra vez',
    		], 422);
    	}

    }

    public function logout(){

    	$token = JWTAuth::getToken();

		try{
		JWTAuth::invalidate($token);

			return response()->json([
				'success' => true,
				'message' => 'Sesion cerrada'
			], 200);

		}catch(JWTExceptions $ex){
			return response()->json([
				'success' => false,
				'message' => 'Sesion no cerrada, porfavor intente de nuevo'
			], 422);
		}
    }

    public function user(Request $request){
    	return response()->json([
				'user' => Auth::user()
			], 200);
    }
}
