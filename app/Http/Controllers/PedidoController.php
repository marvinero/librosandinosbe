<?php

namespace App\Http\Controllers;

use App\pedido;
use App\libro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('pedidos')
        ->join('libros','libros.id','=','pedidos.libro_id')
        ->select('pedidos.*', 'libros.titulo', 'libros.autor', 'libros.editorial','libros.precio_venta','precio_compra')
        ->where('pedidos.'.$columna, 'like', '%' . $filter . '%')->whereNull('pedidos.deleted_at')->orderBy('pedidos.'.$columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'orden' => 'required|max:255'
        ]);
        $data = $request->all();
        $data['fecha']=date("Y-m-d h:i:s", time());
        $libro =  libro::find($data['libro_id']);

        if ($libro->stock >= $data['cantidad']) {
            $libro->stock = $libro->stock - $data['cantidad'];
            $libro->update();
            // DB::beginTransaction();
            $pedido = pedido::create([
                'fecha' => $data['fecha'],
                'orden' => $data['orden'],
                'estado' => 'vigente',
                'reclamo' => $data['reclamo'],
                'cantidad' => $data['cantidad'],
                'pais' => $data['pais'],
                'unico' => $data['unico'],
                'libro_id' => $data['libro_id'],
                'l_cliente_id' => $data['l_cliente_id']
            ]);
            
            $mensaje = "Registrado correctamente";    
        }else{
            $mensaje = "No hay stock suficiente, el stock es ".$libro->stock;
        }

        
        return response()->json(['mensaje' => $mensaje], 200);
        // return response()->json(['mensaje' => $mensaje, 'id' => $pedido->id], 200);
    }


    public function update(Request $request, $id)
    {
        $pedido = pedido::find($id);
        $requestData = $request->only('estado');
        $pedido->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = pedido::find($id);
        $pedido->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }
}
