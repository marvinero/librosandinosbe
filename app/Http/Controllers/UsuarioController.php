<?php

namespace App\Http\Controllers;

use App\usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;


class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('users')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {  $request->validate([
        'username' => 'required|string|max:255|unique:users',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6', 
        'nombre' => 'required|string|max:255',
        'apellido' => 'required|string|max:255'
        ]);

        $data = $request->all();
        // DB::beginTransaction();
        $user = usuario::create([
        'username' => $data['username'],
        'email' => $data['email'],
        'web' => $data['web'],
        'nombre' => $data['nombre'],
        'apellido' => $data['apellido'],
        'empresa' => $data['empresa'],
        'telefono' => $data['telefono'],
        'direccion' => $data['direccion'],
        'ciudad' => $data['ciudad'],
        'pais' => $data['pais'],
        'nivel' => $data['nivel'],
        'password' => Hash::make($data['password']),
    ]);
  //  $token = JWTAuth::fromUser($user);
    $mensaje = "Registrado correctamente";
    return response()->json(['mensaje'=>$mensaje,'id'=>$user->id], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(usuario $usuario){
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(usuario $usuario){
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuario $usuario){
         
        $request->validate([
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6', 
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $usuario->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuario $usuario){
    }
}
