<?php

namespace App\Http\Controllers;

use App\l_detalles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LDetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        $data = DB::table('l_detalles')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'orden' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $l_detalle = l_detalles::create([
            'l_venta_id' => $data['l_venta_id'],
            'libro_id' => $data['libro_id'],
            'orden' => $data['orden'],
            'autor' => $data['autor'],
            'titulo' => $data['titulo'],
            'editorial' => $data['editorial'],
            'cantidad' => $data['cantidad'],
            'precio' => $data['precio'],
            'total' => $data['total']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$l_detalle->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $l_detalle = l_detalles::find($id);
        $request->validate([
            'orden' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $l_detalle->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\l_detalles  $l_detalles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $l_detalle = l_detalles::find($id);
        $l_detalle->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
