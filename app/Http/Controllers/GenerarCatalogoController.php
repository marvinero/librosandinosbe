<?php

namespace App\Http\Controllers;

use App\l_venta;
use App\r_venta;
use App\generarCatalogo;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\SimpleType\Jc;
use App\catalogo;
use App\libro;
use App\materia;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig;
use PhpOffice\PhpSpreadsheet\Style\Border;


class GenerarCatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        $text = $section->addText($request->get('name'));
        $text = $section->addText($request->get('email'));
        $text = $section->addText($request->get('number'),array('name'=>'Arial','size' => 20,'bold' => true));
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Appdividend.docx');
        return response()->download(public_path('Appdividend.docx'));
    }

    public function Wordcito2($ids){

        $idre = json_decode($ids);
        $catalogo = catalogo::whereIn('id',$idre)->get();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        $materiaActual='';
        $materia='';

        $section->addTextBreak(5);
        $section->addImage('img/logo.png',[
            "width"=>250,
            "height"=>120,
            'align' => 'center',
        ]);

        $section->addTextBreak(3);
        $section->addText("Catálogos", array('bold' => true, "size"=>36,'color' => 'gray'), array('align' => 'center'));

        $section->addPageBreak();

        foreach ($catalogo as $key => $val){
            if($key>0){
                $section->addPageBreak();
            }

            foreach($val->libros as $key1 => $value){

                $izquierda=true;
                $materia=$value->materia;

                if($materia!==$materiaActual){

                    $section->addTextBreak(10);
                    $section->addText($val->categoria, array('bold' => true, "size"=>36,'color' => 'gray'), array('align' => 'center'));
                    $section->addText($materia, array('bold' => true, "size"=>36,'color' => 'gray'), array('align' => 'center'));
                    $section->addPageBreak();

                    $materiaActual=$materia;

                }

                $cellRowSpan = array('vMerge' => 'restart');
                $cellRowContinue = array('vMerge' => 'continue');
                $cellColSpan = array('gridSpan' => 2);
                $StyleTextoParrafo=array('name' => 'Tahoma', 'size' => 10);
                $paramsTable = array(
                    'align' => 'justify',
                    'border' => 'single',
                    'tableAlign' => 'center',
                    'borderWidth' => 10,
                    'borderColor' => '000000',
                    'textProperties' => array('Bookman Old Style' => true, 'font' => 'Algerian', 'fontSize' => 18),
                    'tableLayout' => 'autofit',
                    'tableWidth' => array('type' =>'pct', 'value' => 0),
                );


                    $table = $section->addTable([$paramsTable]);
                    $table->addRow();
                    $section->addTextBreak(1);

                    $imagenl= is_file($value->img) ? $value->img : 'img/libroBlanco.png';


                    if(!($key1%2 == 0)){

                        $table->addCell(5000, $cellRowSpan)->addImage($imagenl, array(
                            'align' => 'center',
                            'width'         => 100,
                            'height'        => 150,
                            'marginTop'     => -1,
                            'marginLeft'    => -1,
                            'wrappingStyle' => 'behind'));


                        $c1=$table->addCell(5000, $cellRowSpan);

                        $c1->addText('Título:'. $value->titulo,array('name' => 'Bookman Old Style', 'size' => 12));
                        $c1->addTextBreak(1);
                        $c1->addText('Autor:'. $value->autor,$StyleTextoParrafo);
                        $c1->addText('Resumen:'. $value->resumen_tab_cont,$StyleTextoParrafo);
                        $c1->addText('Descripción:'. $value->descripcion,$StyleTextoParrafo);
                        $c1->addText('Edición:'. $value->edicion,$StyleTextoParrafo);
                        $section->addTextBreak(1);

                    }else{

                        $c1=$table->addCell(5000, $cellRowSpan);
                        $c1->addText('Título:'. $value->titulo,array('name' => 'Bookman Old Style', 'size' => 12));
                        $c1->addTextBreak(1);
                        $c1->addText('Autor:'. $value->autor,$StyleTextoParrafo);
                        $c1->addText('Resumen:'. $value->resumen,$StyleTextoParrafo);
                        $c1->addText('Descripción:'. $value->resumen_tab_cont,$StyleTextoParrafo);
                        $c1->addText('Edición:'. $value->edicion,$StyleTextoParrafo);
                        $table->addCell(5000, $cellRowSpan)->addImage($imagenl, array(
                            'align' => 'center',
                            'width'         => 100,
                            'height'        => 150,
                            'marginTop'     => -1,
                            'marginLeft'    => -1,
                            'wrappingStyle' => 'behind'));
                        $section->addTextBreak(1);
                    }

            }
        }
            $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objectWriter->save(storage_path('TestWordFileAll.docx'));
            //dd($archivos);
            return response()->download(storage_path('TestWordFileAll.docx'));
    }

    public function Wordcito($id){

        $catalogo = catalogo::where('id',$id)->first();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $materiaActual='';

        $section->addTextBreak(5);
        $section->addImage('img/logo.png',[
            "width"=>250,
            "height"=>120,
            'align' => 'center',
        ]);

        $section->addTextBreak(3);
        $section->addText($catalogo->nombre, array('bold' => true, "size"=>36,'color' => 'black'), array('align' => 'center'));
        $section->addText("Catàlogos: ".$catalogo->categoria, array('bold' => true, "size"=>26,'color' => 'gray'), array('align' => 'center'));

           foreach($catalogo->libros as $key1 => $value){

                $izquierda=true;
                $materia=$value->materia;

                if($materia!==$materiaActual){
                    $section->addPageBreak();
                    $section->addTextBreak(10);

                    $section->addText($materia, array('bold' => true, "size"=>36,'color' => 'gray'), array('align' => 'center'));
                    $section->addPageBreak();
                    $materiaActual=$materia;
                }

                $cellRowSpan = array('vMerge' => 'restart');
                $cellRowContinue = array('vMerge' => 'continue');
                $cellColSpan = array('gridSpan' => 2);
                $StyleTextoParrafo=array('name' => 'Tahoma', 'size' => 10);
                $paramsTable = array(
                    'align' => 'justify',
                    'border' => 'single',
                    'tableAlign' => 'center',
                    'borderWidth' => 10,
                    'borderColor' => '000000',
                    'textProperties' => array('Bookman Old Style' => true, 'font' => 'Algerian', 'fontSize' => 18),
                    'tableLayout' => 'autofit',
                    'tableWidth' => array('type' =>'pct', 'value' => 0),
                );

                $table = $section->addTable([$paramsTable]);
                $table->addRow();
                $section->addTextBreak(1);

                $imagenl= is_file($value->img) ? $value->img : 'img/libroBlanco.png';

                if(!($key1%2 == 0)){

                    $table->addCell(5000, $cellRowSpan)->addImage($imagenl, array(
                        'align' => 'center',
                        'width'         => 100,
                        'height'        => 150,
                        'marginTop'     => -1,
                        'marginLeft'    => -1,
                        'wrappingStyle' => 'behind'));


                    $c1=$table->addCell(5000, $cellRowSpan);

                    $c1->addText('Título:'. $value->titulo,array('name' => 'Bookman Old Style', 'size' => 12));
                    $c1->addTextBreak(1);
                    $c1->addText('Autor:'. $value->autor,$StyleTextoParrafo);
                    $c1->addText('Resumen:'. $value->resumen_tab_cont,$StyleTextoParrafo);
                    $c1->addText('Descripción:'. $value->descripcion,$StyleTextoParrafo);
                    $c1->addText('Edición:'. $value->edicion,$StyleTextoParrafo);
                    $section->addTextBreak(1);
                }else{
                    $c1=$table->addCell(5000, $cellRowSpan);

                    $c1->addText('Título:'. $value->titulo,array('name' => 'Bookman Old Style', 'size' => 12));
                    $c1->addTextBreak(1);
                    $c1->addText('Autor:'. $value->autor,$StyleTextoParrafo);
                    $c1->addText('Resumen:'. $value->resumen,$StyleTextoParrafo);
                    $c1->addText('Descripción:'. $value->resumen_tab_cont,$StyleTextoParrafo);
                    $c1->addText('Edición:'. $value->edicion,$StyleTextoParrafo);
                    $table->addCell(5000, $cellRowSpan)->addImage($imagenl, array(
                        'align' => 'center',
                        'width'         => 100,
                        'height'        => 150,
                        'marginTop'     => -1,
                        'marginLeft'    => -1,
                        'wrappingStyle' => 'behind'));
                    $section->addTextBreak(1);
                }
            }

        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        $objectWriter->save(storage_path('TestWordFile.docx'));
        return response()->download(storage_path('TestWordFile.docx'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\generarCatalogo  $generarCatalogo
     * @return \Illuminate\Http\Response
     */
    public function show(generarCatalogo $generarCatalogo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\generarCatalogo  $generarCatalogo
     * @return \Illuminate\Http\Response
     */
    public function edit(generarCatalogo $generarCatalogo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\generarCatalogo  $generarCatalogo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, generarCatalogo $generarCatalogo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\generarCatalogo  $generarCatalogo
     * @return \Illuminate\Http\Response
     */
    public function destroy(generarCatalogo $generarCatalogo)
    {
        //
    }

    public function excelLibro($data){

        //true and false recod=rrido de la I
        $data1 = json_decode($data);
        $venta = l_venta::find($data1->idVenta);
        $datos = $venta->l_detalles;//DB::table('l_detalles')->where('l_venta_id', $data1->idVenta)->get();

        //dd($datos);
        $inicio=20;
        $fin=19+count($datos);

        //make a new spreadsheet object
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1,3);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1,3);
        $tableHead = [
            'font'=>[
                'color'=>[
                    'rgb'=>'FFFFFF'
                ],
                'bold'=>true,
                'size'=>11
            ],
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'F05E23'
                ]
            ],
        ];
        //even row
        $evenRow = [
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '9C9C9C'
                ]
            ]
        ];
        //odd row
        $oddRow = [
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'F4F4F4'
                ]
            ]
        ];
        //get current active sheet (first sheet)
        $spreadsheet->getActiveSheet();

        //drawing image logo.png
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Logo');
        $drawing->setPath('img/librosandinos.png'); // put your path and image here
        $drawing->setCoordinates('B3');
        $drawing->setHeight(153);
        $drawing->getShadow()->setVisible(true);
        //$drawing->getShadow()->setDirection(45);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        //set the value of cell a1 to "Hello World!"

        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F05E23');
        $spreadsheet->getActiveSheet()->getStyle('A42:I42')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F05E23');


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(7);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(8);
        $spreadsheet->getActiveSheet()
                    ->getStyle('A10:I10')
                    ->getBorders()
                    ->getBottom();


        $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
        $spreadsheet->getActiveSheet()->getCell('C4')
                    ->setValue("Libros Andinos!");

        $spreadsheet->getActiveSheet()->getStyle('C4')->getFont()->applyFromArray( [ 'name' => 'Baskerville Old Face', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '808080' ] ] );


        $spreadsheet->getActiveSheet()->setCellValue('C5', 'LibrosAndinos.RaulMontalvo@aol.com');
        $spreadsheet->getActiveSheet()->getCell('C5')->getHyperlink()->setUrl('LibrosAndinos.RaulMontalvo@aol.com');

        $spreadsheet->getActiveSheet()->getCell('C6')
                    ->setValue("Cell: (305)725-5249");
        $spreadsheet->getActiveSheet()->getStyle('C6')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        


        $spreadsheet->getActiveSheet()->getStyle('H6')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H6')
                    ->setValue("DATE");
        $spreadsheet->getActiveSheet()->getStyle('H6')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ]);

        $date = date("Y-m-d");
        $spreadsheet->getActiveSheet()->getStyle('H7')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()
            ->setCellValue('H7', $date);
        $spreadsheet->getActiveSheet()->getStyle('H7')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);


        $spreadsheet->getActiveSheet()->getStyle('H8')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H8')
                    ->setValue("INVOICE NO.");
        $spreadsheet->getActiveSheet()->getStyle('H8')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0000FF' ] ] );
        $spreadsheet->getActiveSheet()->mergeCells("F11:I11");
        $spreadsheet->getActiveSheet()->getCell('F11')
                    ->setValue("<Payment terms (due on receipt, due in X days)>");
        $spreadsheet->getActiveSheet()->getStyle('F11')->getFont()->applyFromArray( [ 'name' => 'Calibri', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );
        $spreadsheet->getActiveSheet()->getStyle('F11')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

        $spreadsheet->getActiveSheet()->getStyle('G8')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );

        $spreadsheet->getActiveSheet()->getStyle('H5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H5')
                    ->setValue("\nINVOICE\n");
        $spreadsheet->getActiveSheet()->getStyle('H5')->getAlignment()->setWrapText(true);
        //Row1
        $spreadsheet->getActiveSheet()->mergeCells("C12:D12");

            $spreadsheet->getActiveSheet()->getCell('B12')
                        ->setValue("From");
            $spreadsheet->getActiveSheet()->getStyle('B12')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ] );

            $spreadsheet->getActiveSheet()->getCell('B14')
                        ->setValue("Libros Andinos");
            $spreadsheet->getActiveSheet()->getCell('B15')
                        ->setValue("P.O. Box 164900");
            $spreadsheet->getActiveSheet()->getCell('B16')
                        ->setValue("Miami, FL 33116");
            $spreadsheet->getActiveSheet()->getCell('B17')
                        ->setValue("(305)378-0089");
            $spreadsheet->getActiveSheet()->getCell('B17')
                        ->setValue("LibrosAndinos.RaulMontalvo@aol.com");
        //EndRow1
        //Row2
        $spreadsheet->getActiveSheet()->mergeCells("E12:F12");
            $spreadsheet->getActiveSheet()->getCell('E12')
                        ->setValue("SHIP TO");
            $spreadsheet->getActiveSheet()->getStyle('E12')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ] );

            $spreadsheet->getActiveSheet()->getCell('E14')
                        ->setValue("Libroary Of Congress");
            $spreadsheet->getActiveSheet()->getCell('E15')
                        ->setValue("<Client Company Name>");
            $spreadsheet->getActiveSheet()->getCell('E16')
                        ->setValue("<Address>");
            $spreadsheet->getActiveSheet()->getCell('E17')
                        ->setValue("<Phone>");
        //EndRow2
        $spreadsheet->getActiveSheet()->mergeCells("A46:H46");
        //$sheet->setCellValue('B12:C12', 'From');

        $spreadsheet->getActiveSheet()->mergeCells("G13:I13");
        $spreadsheet->getActiveSheet()->getStyle('G13')->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('332F2C');

        $spreadsheet->getActiveSheet()->getCell('G13')
              ->setValue("SUSCRIPTION");
        $spreadsheet->getActiveSheet()->getStyle('G13')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('G13')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0A0A0A' ]]);

        $spreadsheet->getActiveSheet()->mergeCells("G14:I15");
        $spreadsheet->getActiveSheet()->getStyle('G14')->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('332F2C');
        $spreadsheet->getActiveSheet()->getCell('G14')
              ->setValue($venta->l_clientes->pais);
        $spreadsheet->getActiveSheet()->getStyle('G14')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('G14')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0A0A0A' ]]);
        //     $spreadsheet->getActiveSheet()->getStyle('H13')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );
        //Table

        $styleArray = [
            'borders' => [
                'vertical' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '7D7F7D'],
                ],
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '7D7F7D'],
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A20:I'.$fin)->applyFromArray($styleArray);


        $spreadsheet->getActiveSheet()->getStyle('A19:I19')->applyFromArray($tableHead);
        $spreadsheet->getActiveSheet()->getStyle('A19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $spreadsheet->getActiveSheet()->getCell('A19')
                    ->setValue("Nª");
        $spreadsheet->getActiveSheet()->getStyle('A19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('A19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


        if ($data1->cod) {
           $spreadsheet->getActiveSheet()->getStyle('B19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $spreadsheet->getActiveSheet()->getCell('B19')
                    ->setValue("Cod");
        $spreadsheet->getActiveSheet()->getStyle('B19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('B19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }


        if ($data1->orden) {
            $spreadsheet->getActiveSheet()->getStyle('C19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            $spreadsheet->getActiveSheet()->getCell('C19')
                    ->setValue("OrderNª");
            $spreadsheet->getActiveSheet()->getStyle('C19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
            $spreadsheet->getActiveSheet()->getStyle('C19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }


        $spreadsheet->getActiveSheet()->getStyle('D19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('D19')
                    ->setValue("Author");
        $spreadsheet->getActiveSheet()->getStyle('D19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('D19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


        $spreadsheet->getActiveSheet()->getStyle('E19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('E19')
                    ->setValue("Tittle");
        $spreadsheet->getActiveSheet()->getStyle('E19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('E19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        $spreadsheet->getActiveSheet()->getStyle('F19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('F19')
                    ->setValue("Publisher");
        $spreadsheet->getActiveSheet()->getStyle('F19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('F19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        if ($data1->cantidad) {
            $spreadsheet->getActiveSheet()->getStyle('G19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getCell('G19')
                    ->setValue("QT");
            $spreadsheet->getActiveSheet()->getStyle('G19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $spreadsheet->getActiveSheet()->getStyle('G19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        }

        if ($data1->precio) {
            $spreadsheet->getActiveSheet()->getStyle('H19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getCell('H19')
                    ->setValue("Unit");
            $spreadsheet->getActiveSheet()->getStyle('H19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $spreadsheet->getActiveSheet()->getStyle('H19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE]);
        }


        $spreadsheet->getActiveSheet()->getStyle('I19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('I19')
                    ->setValue("Price");
        $spreadsheet->getActiveSheet()->getStyle('I19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('I19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        //Table request Data $data

        $row=20;
        foreach($datos as $student){
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$row, $student->id)
                ->setCellValue('B'.$row, $student->cod ? $student->cod :'undefined')
                ->setCellValue('D'.$row, $student->autor)
                ->setCellValue('E'.$row, $student->titulo)
                ->setCellValue('F'.$row, $student->editorial)
                ->setCellValue('I'.$row, $student->total);
                if($data1->cod){
                  $spreadsheet->getActiveSheet()->setCellValue('B'.$row, $student->cod ? $student->cod:'undefined');
                }
                if($data1->orden){
                  $spreadsheet->getActiveSheet()->setCellValue('C'.$row, $student->orden);
                }
                if($data1->cantidad){
                  $spreadsheet->getActiveSheet()->setCellValue('G'.$row, $student->cantidad);
                }
                if ($data1->precio) {
                    $spreadsheet->getActiveSheet()->setCellValue('H'.$row, $student->precio);
                }

            //set row style
            if( $row % 2 == 0 ){
                //even row
                $spreadsheet->getActiveSheet()->getStyle('A'.$row.':I'.$row)->applyFromArray($evenRow);
            }else{
                //odd row
                $spreadsheet->getActiveSheet()->getStyle('A'.$row.':I'.$row)->applyFromArray($oddRow);
            }
            //increment row
            $row++;
        }

        //autofilter
        //define first row and last row
        $firstRow=2;
        $lastRow=$row-1;
        //set the autofilter
        //$spreadsheet->getActiveSheet()->setAutoFilter("A".$firstRow.":F".$lastRow);


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+1))
                    ->setValue("SUBTOTAL");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))->getFont()->applyFromArray( [ 'setFonSize'=>'9', 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+2))
                    ->setValue("DISCOUNT");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+3))
                    ->setValue("SUBTOTAL LESS DISCOUNT");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+5))
                    ->setValue("TOTAL TAX");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+6))
                    ->setValue("SHIPPING/HANDLING");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(12)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+7))
                    ->setValue("Balance Due $");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+7))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FDBCB4');


        $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+1))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+2))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+3))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+4))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+5))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+6))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+7))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+8))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);


        //mark send
            if ($data1->marksend) {
               $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('7D7F7D');
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('C'.($fin+3))
                    ->setValue("MARC RECORDS ");
                    $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('7D7F7D');

        $spreadsheet->getActiveSheet()->getCell('C'.($fin+4))
                        ->setValue("TO BE SENT");
                        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
            }


        //FORMULAS
        //subtotal 32I
        $spreadsheet->getActiveSheet()->setCellValue(
            'I'.($fin+1),
            '=SUM(I20:I'.$fin.') '
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+1))
            ->getStyle()->setQuotePrefix(true);

        //SUBTOTAL LESS DISCOUNT
        $spreadsheet->getActiveSheet()->setCellValue(
            'I'.($fin+3),
            '=I'.($fin+1).'-I'.($fin+2)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+3))
            ->getStyle()->setQuotePrefix(true);
        //totalTax
        $spreadsheet->getActiveSheet()->setCellValue(
        'I'.($fin+5),
        '=I'.($fin+3).'*I'.($fin+4)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+5))
            ->getStyle()->setQuotePrefix(true);

        //Balance Due
        $spreadsheet->getActiveSheet()->setCellValue(
        'I'.($fin+7),
        '=I'.($fin+3).'+I'.($fin+5).'+I'.($fin+6)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+7))
            ->getStyle()->setQuotePrefix(true);

        //$sheet->rangeToArray("A1:A3");
        $writer = new Xlsx($spreadsheet);
        //write the file in current directory
        //$writer->save('LibroFactura'.$data1->idVenta.'.xlsx');
        $writer->save('Invoice-Template-top.xlsx');


            //dd($archivos);
            //return response()->download(public_path('LibroFactura'.$data1->idVenta.'.xlsx'));
            return response()->download(public_path('Invoice-Template-top.xlsx'));

            //redirect to the file
            //echo "<meta http-equiv='refresh' content='0;url=hello world.xlsx'/>";
        }





























   public function excelRevista($data){

        //true and false recod=rrido de la I
        $data1 = json_decode($data);
        $venta = r_venta::find($data1->idVenta);
        $datos = DB::table('r_detalles')->where('r_venta_id', $data1->idVenta)->get();

        //dd($datos);
        $inicio=20;
        $fin=19+count($datos);

        //make a new spreadsheet object
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->getPageMargins()->setTop(1,3);
        $spreadsheet->getActiveSheet()->getPageMargins()->setRight(0);
        $spreadsheet->getActiveSheet()->getPageMargins()->setLeft(0);
        $spreadsheet->getActiveSheet()->getPageMargins()->setBottom(1,3);
        $tableHead = [
            'font'=>[
                'color'=>[
                    'rgb'=>'FFFFFF'
                ],
                'bold'=>true,
                'size'=>11
            ],
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'F05E23'
                ]
            ],
        ];
        //even row
        $evenRow = [
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '9C9C9C'
                ]
            ]
        ];
        //odd row
        $oddRow = [
            'fill'=>[
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'F4F4F4'
                ]
            ]
        ];
        //get current active sheet (first sheet)
        $spreadsheet->getActiveSheet();

        //drawing image logo.png
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Logo');
        $drawing->setPath('img/librosandinos.png'); // put your path and image here
        $drawing->setCoordinates('B3');
        $drawing->setHeight(153);
        $drawing->getShadow()->setVisible(true);
        //$drawing->getShadow()->setDirection(45);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        //set the value of cell a1 to "Hello World!"

        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F05E23');
        $spreadsheet->getActiveSheet()->getStyle('A42:I42')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('F05E23');


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(8);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(13);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(7);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(9);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(8);
        $spreadsheet->getActiveSheet()
                    ->getStyle('A10:I10')
                    ->getBorders()
                    ->getBottom();


        $spreadsheet->getActiveSheet()->mergeCells("A1:H1");
        $spreadsheet->getActiveSheet()->getCell('C4')
                    ->setValue("Libros Andinos!");

        $spreadsheet->getActiveSheet()->getStyle('C4')->getFont()->applyFromArray( [ 'name' => 'Baskerville Old Face', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '808080' ] ] );


        $spreadsheet->getActiveSheet()->setCellValue('C5', 'LibrosAndinos.RaulMontalvo@aol.com');
        $spreadsheet->getActiveSheet()->getCell('C5')->getHyperlink()->setUrl('LibrosAndinos.RaulMontalvo@aol.com');

        $spreadsheet->getActiveSheet()->getCell('C6')
                    ->setValue("Cell: (305)725-5249");
        $spreadsheet->getActiveSheet()->getStyle('C6')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        $spreadsheet->getActiveSheet()->getCell('C7')
                    ->setValue("Office: (305)378-0089");
        $spreadsheet->getActiveSheet()->getStyle('C7')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H7')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H7')
                    ->setValue("DATE");
        $spreadsheet->getActiveSheet()->getStyle('H7')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ] );

        $date = date("Y-m-d");
        $spreadsheet->getActiveSheet()->getStyle('H8')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()
            ->setCellValue('H8', $date);
        $spreadsheet->getActiveSheet()->getStyle('H8')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);


        $spreadsheet->getActiveSheet()->getStyle('H9')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H9')
                    ->setValue("INVOICE NO.");
        $spreadsheet->getActiveSheet()->getStyle('H9')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0000FF' ] ] );
        $spreadsheet->getActiveSheet()->mergeCells("F11:I11");
        // $spreadsheet->getActiveSheet()->getCell('F11')
        //             ->setValue("<Payment terms (due on receipt, due in X days)>");
        $spreadsheet->getActiveSheet()->getStyle('F11')->getFont()->applyFromArray( [ 'name' => 'Calibri', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );
        $spreadsheet->getActiveSheet()->getStyle('F11')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

        $spreadsheet->getActiveSheet()->getStyle('G8')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );

        $spreadsheet->getActiveSheet()->getStyle('H5')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('H5')
                    ->setValue("INVOICE");
        $spreadsheet->getActiveSheet()->getStyle('H5')->getAlignment()->setWrapText(true);
        //Row1
        //$spreadsheet->getActiveSheet()->mergeCells("C12:D12");

            $spreadsheet->getActiveSheet()->getCell('B12')
                        ->setValue("From");
            $spreadsheet->getActiveSheet()->getStyle('B12')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ] );

            $spreadsheet->getActiveSheet()->getStyle('B12')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

            $spreadsheet->getActiveSheet()->getCell('C12')
                        ->setValue("Libros Andinos");
            $spreadsheet->getActiveSheet()->getCell('C13')
                        ->setValue("P.O. Box 164900");
            $spreadsheet->getActiveSheet()->getCell('C14')
                        ->setValue("Miami, FL 33116");
            $spreadsheet->getActiveSheet()->getCell('C15')
                        ->setValue("(305)378-0089");
            $spreadsheet->getActiveSheet()->getCell('C16')
                        ->setValue("LibrosAndinos.RaulMontalvo@aol.com");
        //EndRow1
        //Row2
        //$spreadsheet->getActiveSheet()->mergeCells("F12:G12");
            $spreadsheet->getActiveSheet()->getCell('F12')
                        ->setValue("TO");
            $spreadsheet->getActiveSheet()->getStyle('F12')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '1E2460' ] ] );
            $spreadsheet->getActiveSheet()->getStyle('F12')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

            $spreadsheet->getActiveSheet()->getCell('G12')
                        ->setValue("Libroary Of Congress");
            $spreadsheet->getActiveSheet()->getCell('G13')
                        ->setValue("<Client Company Name>");
            $spreadsheet->getActiveSheet()->getCell('G14')
                        ->setValue("<Address>");
            $spreadsheet->getActiveSheet()->getCell('G15')
                        ->setValue("<Phone>");
        //EndRow2
        $spreadsheet->getActiveSheet()->mergeCells("A46:H46");
        //$sheet->setCellValue('B12:C12', 'From');

        // $spreadsheet->getActiveSheet()->mergeCells("G13:I13");
        // $spreadsheet->getActiveSheet()->getStyle('G13')->getFont()->setBold(true)
        //                                 ->setName('Roboto')
        //                                 ->setSize(18)
        //                                 ->getColor()->setRGB('332F2C');

        // $spreadsheet->getActiveSheet()->getCell('G13')
        //       ->setValue("SUSCRIPTION");
        // $spreadsheet->getActiveSheet()->getStyle('G13')
        //     ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $spreadsheet->getActiveSheet()->getStyle('G13')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0A0A0A' ]]);

        // $spreadsheet->getActiveSheet()->mergeCells("G14:I15");
        // $spreadsheet->getActiveSheet()->getStyle('G14')->getFont()->setBold(true)
        //                                 ->setName('Roboto')
        //                                 ->setSize(18)
        //                                 ->getColor()->setRGB('332F2C');
        // $spreadsheet->getActiveSheet()->getCell('G14')
        //       ->setValue($venta->r_clientes->pais);
        // $spreadsheet->getActiveSheet()->getStyle('G14')
        //     ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $spreadsheet->getActiveSheet()->getStyle('G14')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '0A0A0A' ]]);
        //     $spreadsheet->getActiveSheet()->getStyle('H13')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE, 'color' => [ 'rgb' => '7D7F7D' ] ] );
        //Table

        $styleArray = [
            'borders' => [
                'vertical' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '7D7F7D'],
                ],
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '7D7F7D'],
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A20:I'.$fin)->applyFromArray($styleArray);


        $spreadsheet->getActiveSheet()->getStyle('A19:I19')->applyFromArray($tableHead);
        $spreadsheet->getActiveSheet()->getStyle('A19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $spreadsheet->getActiveSheet()->getCell('A19')
                    ->setValue("Nª");
        $spreadsheet->getActiveSheet()->getStyle('A19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('A19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


        $spreadsheet->getActiveSheet()->mergeCells("C19:D19");

               $spreadsheet->getActiveSheet()->getStyle('B19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            $spreadsheet->getActiveSheet()->getCell('B19')
                        ->setValue("P.O");
            $spreadsheet->getActiveSheet()->getStyle('B19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
            $spreadsheet->getActiveSheet()->getStyle('B19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            if ($data1->orden) {
                $spreadsheet->getActiveSheet()->getStyle('C19')
                    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $spreadsheet->getActiveSheet()->getCell('C19')
                        ->setValue("Tittle");
                $spreadsheet->getActiveSheet()->getStyle('C19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
                $spreadsheet->getActiveSheet()->getStyle('C19')
                    ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            }


        // $spreadsheet->getActiveSheet()->getStyle('D19')
        //     ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // $spreadsheet->getActiveSheet()->getCell('D19')
        //             ->setValue("Author");
        // $spreadsheet->getActiveSheet()->getStyle('D19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        // $spreadsheet->getActiveSheet()->getStyle('D19')
        //     ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


        $spreadsheet->getActiveSheet()->getStyle('E19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('E19')
                    ->setValue("Sending Now");
        $spreadsheet->getActiveSheet()->getStyle('E19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('E19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        $spreadsheet->getActiveSheet()->getStyle('F19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('F19')
                    ->setValue("Country");
        $spreadsheet->getActiveSheet()->getStyle('F19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('F19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        if ($data1->cantidad) {
            $spreadsheet->getActiveSheet()->getStyle('G19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getCell('G19')
                    ->setValue("QT");
            $spreadsheet->getActiveSheet()->getStyle('G19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $spreadsheet->getActiveSheet()->getStyle('G19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        }

        if ($data1->precio) {
            $spreadsheet->getActiveSheet()->getStyle('H19')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getActiveSheet()->getCell('H19')
                    ->setValue("Unit");
            $spreadsheet->getActiveSheet()->getStyle('H19')
                ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $spreadsheet->getActiveSheet()->getStyle('H19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE]);
        }


        $spreadsheet->getActiveSheet()->getStyle('I19')
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('I19')
                    ->setValue("Price");
        $spreadsheet->getActiveSheet()->getStyle('I19')
            ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $spreadsheet->getActiveSheet()->getStyle('I19')->getFont()->applyFromArray( [ 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        //Table request Data $data

        $row=20;
        foreach($datos as $student){
            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$row, $student->id)
                ->setCellValue('B'.$row, $student->orden)
                //->setCellValue('D'.$row, $student->autor)
                ->setCellValue('C'.$row, $student->titulo)
                //->setCellValue('F'.$row, $student->editorial)
                ->setCellValue('I'.$row, $student->total);
                if($data1->orden){
                  $spreadsheet->getActiveSheet()->setCellValue('B'.$row, $student->orden);
                }

                if($data1->cantidad){
                  $spreadsheet->getActiveSheet()->setCellValue('G'.$row, $student->cantidad);
                }
                if ($data1->precio) {
                    $spreadsheet->getActiveSheet()->setCellValue('H'.$row, $student->precio);
                }

            //set row style
            if( $row % 2 == 0 ){
                //even row
                $spreadsheet->getActiveSheet()->getStyle('A'.$row.':I'.$row)->applyFromArray($evenRow);
            }else{
                //odd row
                $spreadsheet->getActiveSheet()->getStyle('A'.$row.':I'.$row)->applyFromArray($oddRow);
            }
            //increment row
            $row++;
        }

        //autofilter
        //define first row and last row
        $firstRow=2;
        $lastRow=$row-1;
        //set the autofilter
        //$spreadsheet->getActiveSheet()->setAutoFilter("A".$firstRow.":F".$lastRow);


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+1))
                    ->setValue("SUBTOTAL");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+1))->getFont()->applyFromArray( [ 'setFonSize'=>'9', 'name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+2))
                    ->setValue("DISCOUNT");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+2))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+3))
                    ->setValue("SUBTOTAL LESS DISCOUNT");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+3))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+5))
                    ->setValue("TOTAL TAX");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+5))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(8)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+6))
                    ->setValue("SHIPPING/HANDLING");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+6))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );

        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(12)
                                        ->getColor()->setRGB('1E2460');
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getCell('H'.($fin+7))
                    ->setValue("Balance Due $");
        $spreadsheet->getActiveSheet()->getStyle('H'.($fin+7))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );


        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+7))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FDBCB4');


        $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+1))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+2))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+3))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+4))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+5))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+6))->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+7))->getNumberFormat()
        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        $spreadsheet->getActiveSheet()->getStyle('I'.($fin+8))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);


        //mark send
            if ($data1->marksend) {
               $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('7D7F7D');
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getCell('C'.($fin+3))
                    ->setValue("MARC RECORDS ");
                    $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))->getFont()->setBold(true)
                                        ->setName('Roboto')
                                        ->setSize(18)
                                        ->getColor()->setRGB('7D7F7D');

        $spreadsheet->getActiveSheet()->getCell('C'.($fin+4))
                        ->setValue("TO BE SENT");
                        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))
            ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+3))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
        $spreadsheet->getActiveSheet()->getStyle('C'.($fin+4))->getFont()->applyFromArray( ['name' => 'Roboto', 'bold' => TRUE, 'italic' => FALSE, 'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_NONE, 'strikethrough' => FALSE] );
            }


        //FORMULAS
        //subtotal 32I
        $spreadsheet->getActiveSheet()->setCellValue(
            'I'.($fin+1),
            '=SUM(I20:I'.$fin.') '
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+1))
            ->getStyle()->setQuotePrefix(true);

        //SUBTOTAL LESS DISCOUNT
        $spreadsheet->getActiveSheet()->setCellValue(
            'I'.($fin+3),
            '=I'.($fin+1).'-I'.($fin+2)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+3))
            ->getStyle()->setQuotePrefix(true);
        //totalTax
        $spreadsheet->getActiveSheet()->setCellValue(
        'I'.($fin+5),
        '=I'.($fin+3).'*I'.($fin+4)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+5))
            ->getStyle()->setQuotePrefix(true);

        //Balance Due
        $spreadsheet->getActiveSheet()->setCellValue(
        'I'.($fin+7),
        '=I'.($fin+3).'+I'.($fin+5).'+I'.($fin+6)
        );
        $spreadsheet->getActiveSheet()->getCell('I'.($fin+7))
            ->getStyle()->setQuotePrefix(true);

        //$sheet->rangeToArray("A1:A3");
        $writer = new Xlsx($spreadsheet);
        //write the file in current directory
        $writer->save('RevistaFactura'.$data1->idVenta.'.xlsx');
        return response()->download(public_path('RevistaFactura'.$data1->idVenta.'.xlsx'));
    }
}
