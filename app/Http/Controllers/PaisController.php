<?php

namespace App\Http\Controllers;

use App\pais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('pais')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $pais = pais::create([
            'abreviatura' => $data['abreviatura'],
            'nombre' => $data['nombre'],
            'clientes' => $data['clientes']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$pais->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $pais = pais::find($id);
        $request->validate([
            'nombre' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $pais->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pais = pais::find($id);
        $pais->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
