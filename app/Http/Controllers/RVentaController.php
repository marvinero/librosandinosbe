<?php

namespace App\Http\Controllers;

use App\r_venta;
use Illuminate\Http\Request;

class RVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $limit = $request->get('limit');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        $data = r_venta::whereRaw('LOWER('.$columna.') like ? ', $ff)
                ->orderBy($columna,$order)->get();
      
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'aproval' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $r_venta = r_venta::create([
            'factura' => $data['factura'],
            'fecha' => $data['fecha'],
            'r_cliente_id' => $data['r_cliente_id'],
            'r_cliente_nombre' => $data['r_cliente_nombre'],
            'direccion' => $data['direccion'],
            'aproval' => $data['aproval'],
            'cantidad' => $data['cantidad'],
            'tbook' => $data['tbook'],
            'porcentaje' => $data['porcentaje'],
            'discount' => $data['discount'],
            'subtotal' => $data['subtotal'],
            'taxes' => $data['taxes'],
            'impuesto' => $data['impuesto'],
            'envio' => $data['envio'],
            'total' => $data['total'],
            'glosa' => $data['glosa'],
            'head' => $data['head'],
            'foot' => $data['foot'],
            'mark' => $data['mark'],
            'view' => $data['view'],
            'user_name' => $data['user_name'],
            'unico' => $data['unico'],
            'registro' => $data['registro']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$r_venta->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $r_venta = r_venta::find($id);
        $request->validate([
            'aproval' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $r_venta->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r_venta  $r_venta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r_venta = r_venta::find($id);
        $r_venta->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
