<?php

namespace App\Http\Controllers;

use App\r_clientes;
use Illuminate\Http\Request;

class RClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        $data = r_clientes::whereRaw('LOWER('.$columna.') like ? ', $ff)
                ->orderBy($columna,$order)->get();
     
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'empresa' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $r_cliente = r_clientes::create([
            'username' => $data['username'],
            'password' => $data['password'],
            'email' => $data['email'],
            'web' => $data['web'],
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'empresa' => $data['empresa'],
            'telefono' => $data['telefono'],
            'direccion' => $data['direccion'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$r_cliente->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $r_cliente = r_clientes::find($id);
        $request->validate([
            'empresa' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $r_cliente->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r_clientes  $r_cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r_cliente = r_clientes::find($id);
        $r_cliente->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
