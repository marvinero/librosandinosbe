<?php

namespace App\Http\Controllers;

use App\oferta;
use Illuminate\Http\Request;

class OfertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        $data = oferta::whereRaw('LOWER('.$columna.') like ? ', $ff)
                ->orderBy($columna,$order)->paginate($limit);
      /*   foreach ($data as $key => $value) {
            $value->organizativaunidad;
        } */
        return response()->json(['data'=>$data], 200); 
    }

    public function store(Request $request)
    {
        /* $request->validate([
            'titulo' => 'required|max:255'
        ]); */
        $data = $request->all();
        // DB::beginTransaction();
        $oferta = oferta::create([
            'categoria' => $data['categoria'],
            'grupo' => $data['grupo'],
            'precio' => $data['precio'],
            'unico' => $data['unico'],
            'libro_id' => $data['libro_id'],
            'user_id' => $data['user_id']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$oferta->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $oferta = oferta::find($id);
        /* $request->validate([
            'titulo' => 'required|string|max:255'
        ]); */
        $requestData = $request->all();
        $oferta->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oferta = oferta::find($id);
        $oferta->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
