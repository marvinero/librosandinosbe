<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\libro;
use App\catalogo;
use Illuminate\Http\Request;
use Image;
use Carbon\Carbon;
use File;

class LibroController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $tabla = $request->get('tabla');
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        if ($columna == 'catalogo_id') {
            $data = DB::table('libros')->where($columna, $filter)->orderBy($columna,$order)->get();
            
        }else{
            $data = DB::table('libros')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        }
        
        foreach ($data as $key => $value) {
            if ($value->img) {
                $value->img = asset($value->img);
            }
             
        }
        return response()->json($data, 200); 
    }

    public function store(Request $request){
        $request->validate([
            'titulo' => 'required|max:255']);
        $path='';
        $new_name='';
        $data = $request->all();
        $catalogo=catalogo::find($data['catalogo_id']);
        $cod=$catalogo->categoria;
        

        $numval=count($catalogo->libros)+1;
        $numlenght = strlen((string)$numval);        
        $numero = '';
        $valCod='';
        if($numlenght == 1){

            $numero=((string)"00$numval");
        }else{
            if($numlenght == 2){
                $numero=((string)"0$numval");

            }else{
                $numero=((string)$numval);

            }          
        }
        if ($catalogo->estado!='cerrado') {

            $valCod = $cod."TMP$numero";

        }else{
            $valCod = $cod.((string)$numero);
        }


        if(!$request->hasFile('img')) {
            $mensaje = "Archivo no encontrado";
            
        }
        else{
                $file = $request->file('img');
                $extension = $file->getClientOriginalExtension();
                $image = Image::make($file)->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $imagen = str_replace(':','-',str_replace(' ','-',Carbon::now()->toDateTimeString().'.'.$extension)); 
                $path  = 'storage/archivos';
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $img = $path.'/'.$imagen; 
                if($image->save($img)) {
                    if(isset($data['id'])){
                        $data['img'] =  $img;
                        $libro = libro::find($data['id']);
                       // $requestData['img']=$new_name;
                        $libro->update($data);
                        $mensaje = "Actualizado correctamente";
                    }else{
                       // DB::beginTransaction();
                        $libro = libro::create([
                            'catalogo_id' => $data['catalogo_id'],
                            'cod' => $valCod,
                            'tipo' => $data['tipo'],
                            'promocion' => $data['promocion'],
                            'ciudad' => $data['ciudad'],
                            'autor' => $data['autor'],
                            'autor_i' => $data['autor_i'],
                            'titulo' => $data['titulo'],
                            'coleccion' => $data['coleccion'],
                            'n_volumen' => $data['n_volumen'],
                            't_volumen' => $data['t_volumen'],
                            'isbn' => $data['isbn'],
                            'paginas' => $data['paginas'],
                            'editorial' => $data['editorial'],
                            'materia' => $data['materia'],
                            'ciudad' => $data['ciudad'],
                            'pais' => $data['pais'],
                            'edicion' => $data['edicion'],
                            'info_descuento' => $data['info_descuento'],
                            'fecha_pub' => $data['fecha_pub'],
                            'fecha_iso' => $data['fecha_iso'],
                            'impresion' => $data['impresion'],
                            'idioma' => $data['idioma'],
                            'notas' => $data['notas'],
                            'resumen_catalogo' => $data['resumen_catalogo'],
                            'resumen_tab_cont' => $data['resumen_tab_cont'],
                            'ref_bibliografia' => $data['ref_bibliografia'],
                            'descriptores' => $data['descriptores'],
                            'cat_geografica' => $data['cat_geografica'],
                            'desc_compra' => $data['desc_compra'],
                            'tipo_cambio' => $data['tipo_cambio'],
                            'precio_mn' => $data['precio_mn'],
                            'precio_compra_mn' => $data['precio_compra_mn'],
                            'precio_compra' => $data['precio_compra'],
                            'precio_oferta' => $data['precio_oferta'],
                            'precio_venta' => $data['precio_venta'],
                            'fecha_compra' => $data['fecha_compra'],
                            'formato' => $data['formato'],
                            'medidas' => $data['medidas'],
                            'stock' => $data['stock'],
                            'por_comprar' => $data['por_comprar'],
                            'barcode' => $data['barcode'],
                            'd_responsable' => $data['d_responsable'],
                            'img'=>$img
                        ]);
                        $mensaje = "Registrado correctamente"; 

                    }
                }
                else{
                        $mensaje = "Error al guardar la imagen";
                    
                    }
            }


           
        return response()->json(['mensaje'=>$mensaje], 200);
    }

  
    public function update(Request $request, $id)
        {
        $request->validate([
            'titulo' => 'required|max:255'
        ]);

        if(!$request->hasFile('img')) {
            $mensaje = "Archivo no encontrado";
        }else{
            $img = $request->file('img');
            $extension = $img->getClientOriginalExtension();
            $new_name = rand(0,10)."-".date('his').".".$extension;
            $path ='storage/archivos/';
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $img->move($path,$new_name);  
            $request->img=$path.'/'.$new_name;

        }

        $libro = libro::find($id);
        $requestData = $request->all();
        $libro->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\libro  $libro
     * @return \Illuminate\Http\Response
     */

    

    public function destroy($id){

        $libro = libro::find($id);
        $libro->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
