<?php

namespace App\Http\Controllers;

use App\numero;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class NumeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('numeros')
        ->join('revistas','revistas.id','=','numeros.revista_id')
        ->select('numeros.*', 'revistas.titulo')
        ->where('numeros.'.$columna, 'like', '%' . $filter . '%')->whereNull('numeros.deleted_at')->orderBy('numeros.'.$columna, $order)->get();
        
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'num' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $numero = numero::create([
            'id' => $data['id'],
            'num' => $data['num'],
            'revista_id' => $data['revista_id'],
            'orden_numero' => $data['orden_numero'],
            'numero' => $data['numero'],
            'tomo' => $data['tomo'],
            'volumen' => $data['volumen'],
            'fecha' => $data['fecha'],
            'descripcion' => $data['descripcion'],
            'precio' => $data['precio'],
            'registro' => $data['registro'],
            'orden' => $data['orden']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$numero->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $numero = numero::find($id);
        $request->validate([
            'num' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $numero->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\numero  $numero
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $numero = numero::find($id);
        $numero->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
