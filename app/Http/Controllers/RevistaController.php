<?php

namespace App\Http\Controllers;

use App\revista;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');       
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('revistas')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $revista = revista::create([
            'titulo' => $data['titulo'],
            'editorial' => $data['editorial'],
            'isbn_issn' => $data['isbn_issn'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais'],
            'idioma' => $data['idioma'],
            'formato' => $data['formato'],
            'medidas' => $data['medidas'],
            'cantidad' => $data['cantidad'],
            'registro' => $data['registro']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$revista->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $revista = revista::find($id);
        $request->validate([
            'titulo' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $revista->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\revista  $revista
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $revista = revista::find($id);
        $revista->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
