<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ConsultarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('libros')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        return response()->json($data, 200); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\consultar  $consultar
     * @return \Illuminate\Http\Response
     */
    public function show(consultar $consultar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\consultar  $consultar
     * @return \Illuminate\Http\Response
     */
    public function edit(consultar $consultar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\consultar  $consultar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, consultar $consultar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\consultar  $consultar
     * @return \Illuminate\Http\Response
     */
    public function destroy(consultar $consultar)
    {
        //
    }
}
