<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use JWTAuth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->json()->all();
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json( compact('token') );
    }
    
    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function index(Request $request){
        //usar rando 10 
        $limit = $request->get('limit');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        $data = User::whereRaw('LOWER('.$columna.') like ? ', $ff)
                ->orderBy($columna,$order)->get();
        foreach ($data as $key => $value) {
            $value->organizativaunidad;
        }
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
            $validator = Validator::make($request->json()->all() , [
            'username' => 'required|string|max:255|',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6', 
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create([
         //   'username' => $request->json()->get('username'),
            'email' => $request->json()->get('email'),
            'password' => Hash::make($request->json()->get('password')),
        ]);
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('username','token'),201);
    }


    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6', 
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            ]);

            $data = $request->all();
            // DB::beginTransaction();
            $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'web' => $data['web'],
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'empresa' => $data['empresa'],
            'telefono' => $data['telefono'],
            'direccion' => $data['direccion'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais'],
            'nivel' => $data['nivel'],
            'password' => Hash::make($data['password']),
        ]);
        $token = JWTAuth::fromUser($user);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$user->id], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $user = User::findOrFail($id);
        $request->validate([
            'nombre' => 'required|string|max:255',
            'email' => ['required',Rule::unique('users')->ignore($id)],
            'username' => ['required',Rule::unique('users')->ignore($id)],
            // 'password' => 'required|string|min:6', 
            'rol' => 'required',
            'organizativa_unidad_id' => 'required'
        ]);
        $data = $request->all();
        $user->nombre = $data['nombre'];
        $user->email = $data['email'];
        $user->telefono = $data['telefono'];
        $user->celular = $data['celular'];
        $user->username = $data['username'];
        if($user->password){
           $user->password = Hash::make($data['password']); 
        } 
        $user->rol = $data['rol'];
        $user->organizativa_unidad_id = $data['organizativa_unidad_id'];
        $user->update();
        $mensaje = "Actualizado correctamente";

        return response()->json(['mensaje'=>$mensaje], 200); */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       /*  $user = User::find($id);
        $user->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200); */
    }
}
