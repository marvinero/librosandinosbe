<?php

namespace App\Http\Controllers;

use App\l_venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('l_ventas')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'aproval' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $l_venta = l_venta::create([
            'factura' => $data['factura'],
            'l_cliente_id' => $data['l_cliente_id'],
            'l_cliente_nombre' => $data['l_cliente_nombre'],
            'direccion' => $data['direccion'],
            'aproval' => $data['aproval'],
            'cantidad' => $data['cantidad'],
            'tbook' => $data['tbook'],
            'porcentaje' => $data['porcentaje'],
            'discount' => $data['discount'],
            'subtotal' => $data['subtotal'],
            'taxes' => $data['taxes'],
            'impuesto' => $data['impuesto'],
            'envio' => $data['envio'],
            'total' => $data['total'],
            'glosa' => $data['glosa'],
            'head' => $data['head'],
            'foot' => $data['foot'],
            'mark' => $data['mark'],
            'view' => $data['view'],
            'user_name' => $data['user_name'],
            'unico' => $data['unico'],
            'registro' => $data['registro']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$l_venta->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $l_venta = l_venta::find($id);
        $requestData = $request->all();
        $l_venta->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\l_venta  $l_venta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $l_venta = l_venta::find($id);
        $l_venta->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
