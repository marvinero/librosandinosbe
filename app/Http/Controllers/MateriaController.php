<?php

namespace App\Http\Controllers;

use App\materia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ValidateRequests;
class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('materias')->where($columna,'like', '%'.$filter.'%')->orderBy($columna,$order)->get();
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'descripcion' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $materia = materia::create([
            'descripcion' => $data['descripcion'],
            'grupo' => $data['grupo'],
            'tema' => $data['tema'],
            'orden' => $data['orden'],
            'pais' => $data['pais'],
            'tipo' => $data['tipo'],
            'orden_t' => $data['orden_t']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$materia->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $materia = materia::find($id);
        $request->validate([
            'descripcion' => 'required|max:255'
        ]);
        $requestData = $request->all();
        $materia->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $materia = materia::find($id);
        $materia->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
