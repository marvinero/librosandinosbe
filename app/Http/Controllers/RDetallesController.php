<?php

namespace App\Http\Controllers;

use App\r_detalles;
use App\r_venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RDetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        $data = DB::table('r_detalles')->where($columna, 'like', '%' . $filter . '%')->orderBy($columna, $order)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request){
        $request->validate([
            'orden' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $r_detalles = r_detalles::create([
            'r_venta_id' => $data['r_venta_id'],
            'numero_id' => $data['numero_id'],
            'orden' => $data['orden'],
            'autor' => $data['autor'],
            'titulo' => $data['titulo'],
            'editorial' => $data['editorial'],
            'cantidad' => $data['cantidad'],
            'precio' => $data['precio'],
            'total' => $data['total'],
            'unico' => $data['unico'],
            'registro' => $data['registro']
        ]);
        $mensaje = "Registrado correctamente";
        $r_venta = r_venta::find($data['r_venta_id']);
        $r_venta->cantidad += $data['cantidad'];
        $r_venta->subtotal += $data['total'];
        //falta porcentaje
        $r_venta->total = (($r_venta->subtotal-$r_venta->discount)+($r_venta->impuesto/100))+$r_venta->envio;
        $r_venta->update();

        return response()->json(['mensaje'=>$mensaje], 200);
    }

  
    public function update(Request $request, $id){
        $r_detalles = r_detalles::find($id);
        $request->validate([
            'orden' => 'required|max:255'
        ]);
        $requestData = $request->all();
        $r_detalles->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\revista  $revista
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $revista = revista::find($id);
        $revista->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
