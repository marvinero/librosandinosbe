<?php

namespace App\Http\Controllers;

use App\numero;
use App\r_pedidos;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
class RPedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $tabla = $request->get('tabla');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        
        $ff = $request->get('ff', $filter == '' ? '%%' : '%' . trim(strtolower($filter)) . '%');
        //$data = libro::whereRaw('LOWER('.$columna.') like ? ', $ff)->orderBy($columna,$order)->paginatelimit($);
        $data = DB::table('r_pedidos')
        ->join('numeros','numeros.id','=','r_pedidos.numero_id')
        ->join('revistas','revistas.id', '=', 'numeros.revista_id')
        ->select('r_pedidos.*', 'numeros.num','numeros.precio', 'revistas.titulo','numeros.numero')
        ->where('r_pedidos.'.$columna, 'like', '%' . $filter . '%')->whereNull('r_pedidos.deleted_at')->orderBy('r_pedidos.'.$columna, $order)->get();
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'orden' => 'required|max:255'
        ]);
        $data = $request->all();
        $data['fecha']=date("Y-m-d h:i:s", time());
        $numero = numero::find($data['numero_id']);

        if($numero->num >= $data['cantidad']){
            $numero->num = $numero->num - $data['cantidad'];
            $numero->update();    
            $pedido = r_pedidos::create([
                'orden' => $data['orden'],
                'estado' => 'vigente',
                'cantidad' => $data['cantidad'],
                // 'editorial' => $data['editorial'],
                // 'titulo' => $data['titulo'],
                // 'autor' => $data['autor'],
                // 'total' => $data['total'],
                // 'precio' => $data['precio'],
                // 'unico' => $data['unico'],
                'r_cliente_id' => $data['r_cliente_id'],
                'numero_id' => $data['numero_id']
            ]);

        $mensaje = "Registrado correctamente";

    }else{
        $mensaje = "No hay stock suficiente, el stock es ".$numero->num;
    }
        return response()->json(['mensaje' => $mensaje], 200);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\r_pedidos  $r_pedidos
     * @return \Illuminate\Http\Response
     */
    public function show(r_pedidos $r_pedidos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r_pedidos  $r_pedidos
     * @return \Illuminate\Http\Response
     */
    public function edit(r_pedidos $r_pedidos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r_pedidos  $r_pedidos
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $pedido = r_pedidos::find($id);
        $requestData = $request->only('estado');
        $pedido->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pedido  $pedido
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = r_pedidos::find($id);
        $pedido->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje' => $mensaje], 200);
    }
}
