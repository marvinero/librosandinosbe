<?php

namespace App\Http\Controllers;

use App\suscripcion;
use Illuminate\Http\Request;

class SuscripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit');
        //Order
        $columna = $request->get('columna');
        $order = $request->get('order');
        $filter = $request->get('filter');
        $ff = $request->get('ff', $filter==''?'%%':'%'.trim(strtolower($filter)).'%');
        $data = suscripcion::whereRaw('LOWER('.$columna.') like ? ', $ff)
                ->orderBy($columna,$order)->get();
       
        return response()->json($data, 200); 
    }

    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required|max:255'
        ]);
        $data = $request->all();
        // DB::beginTransaction();
        $suscripcion = suscripcion::create([
            
            'r_cliente_id' => $data['r_cliente_id'],
            'revista_id' => $data['revista_id'],
            'orden' => $data['orden'],
            'titulo' => $data['titulo'],
            'editorial' => $data['editorial'],
            'estado' => $data['estado'],
            'ciudad' => $data['ciudad'],
            'pais' => $data['pais'],
            'registro' => $data['registro']
        ]);
        $mensaje = "Registrado correctamente";
        return response()->json(['mensaje'=>$mensaje,'id'=>$suscripcion->id], 200);
    }

  
    public function update(Request $request, $id)
    {
        $suscripcion = suscripcion::find($id);
        $request->validate([
            'titulo' => 'required|string|max:255'
        ]);
        $requestData = $request->all();
        $suscripcion->update($requestData);
        $mensaje = "Actualizado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\suscripcion  $suscripcion
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $suscripcion = suscripcion::find($id);
        $suscripcion->delete();
        $mensaje = "Eliminado correctamente";
        return response()->json(['mensaje'=>$mensaje], 200);
    }
}
