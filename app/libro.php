<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class libro extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    public function materias()
    {
        return $this->belongsTo(materia::class);
    }
    public function pais()
    {
        return $this->belongsTo(pais::class);
    }
    public function catalogos()
    {
        return $this->belongsTo(catalogo::class);
    }
    public function pedidos()
    {
        return $this->hasMany(pedido::class);
    }
    public function ofertas()
    {
        return $this->hasMany(oferta::class);
    }
}
