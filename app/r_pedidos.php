<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class r_pedidos extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function numeros()
    {
        return $this->belongsTo(numero::class);
    }
    public function r_cliente()
    {
        return $this->belongsTo(r_clientes::class);
    }
}
