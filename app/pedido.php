<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class pedido extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function libros()
    {
        return $this->belongsTo(libro::class);
    }
    public function l_cliente()
    {
        return $this->belongsTo(l_clientes::class);
    }
}
