<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class l_venta extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function l_clientes()
    {
        return $this->belongsTo(l_clientes::class,'l_cliente_id');
    }
    public function l_detalles()
    {
        return $this->hasMany(l_detalles::class);
    }
}
