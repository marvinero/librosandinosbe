<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class revista extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function suscripcions()
    {
        return $this->hasMany(suscripcion::class);
    }
    public function numeros()
    {
        return $this->hasMany(numero::class);
    }
}
