<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class numero extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public function revista()
    {
        return $this->belongsTo(revista::class);
    }
    public function r_detalles()
    {
        return $this->hasMany(r_detalles::class);
    }
}
