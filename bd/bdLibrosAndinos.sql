

-- Volcando estructura para tabla librosandinos.catalogos
CREATE TABLE IF NOT EXISTS `catalogos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `periodo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gestion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `catalogos_user_id_foreign` (`user_id`),
  CONSTRAINT `catalogos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.catalogos: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `catalogos` DISABLE KEYS */;
INSERT INTO `catalogos` (`id`, `categoria`, `tipo`, `registro`, `estado`, `nombre`, `periodo`, `gestion`, `file`, `pais`, `send`, `text`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'CAT0000001', 'blanket', NULL, 'abierto', 'Bibliografia Boliviana', '123', '123', 'cerrado', 'Venezuela', NULL, NULL, 1, NULL, '2019-11-20 04:12:19', '2019-12-05 05:31:43'),
	(2, 'CAT000002', 'blanket', NULL, 'abierto', 'Bibliografia Peruana', 'Noviembre-Diciembre', '2019', 'cerrado', 'Ecuador', NULL, NULL, 1, NULL, '2019-12-04 20:08:58', '2019-12-05 05:31:19'),
	(3, 'CAT005', 'blanket', NULL, 'abierto', 'Bibliografia Boliviana1', 'Septiembre-Diciembre', '2019', 'corto', 'Colombia', NULL, NULL, 1, NULL, '2019-12-12 20:50:21', '2019-12-12 20:50:21');
/*!40000 ALTER TABLE `catalogos` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.libros
CREATE TABLE IF NOT EXISTS `libros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `catalogo_id` bigint(20) unsigned NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promocion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor_i` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coleccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_volumen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_volumen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isbn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paginas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editorial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edicion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_descuento` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_pub` date DEFAULT NULL,
  `fecha_iso` date DEFAULT NULL,
  `impresion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idioma` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resumen_catalogo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resumen_tab_cont` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_bibliografia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptores` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `materia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_geografica` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_compra` decimal(8,2) DEFAULT NULL,
  `tipo_cambio` decimal(8,2) DEFAULT NULL,
  `precio_mn` decimal(8,2) DEFAULT NULL,
  `precio_compra_mn` decimal(8,2) DEFAULT NULL,
  `precio_compra` decimal(8,2) DEFAULT NULL,
  `precio_oferta` decimal(8,2) DEFAULT NULL,
  `precio_venta` decimal(8,2) DEFAULT NULL,
  `fecha_compra` date DEFAULT NULL,
  `formato` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medidas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `por_comprar` int(11) DEFAULT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `d_responsable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `libros_catalogo_id_foreign` (`catalogo_id`),
  CONSTRAINT `libros_catalogo_id_foreign` FOREIGN KEY (`catalogo_id`) REFERENCES `catalogos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.libros: ~49 rows (aproximadamente)
/*!40000 ALTER TABLE `libros` DISABLE KEYS */;
INSERT INTO `libros` (`id`, `catalogo_id`, `tipo`, `promocion`, `autor`, `autor_i`, `titulo`, `coleccion`, `n_volumen`, `t_volumen`, `isbn`, `paginas`, `editorial`, `ciudad`, `pais`, `edicion`, `info_descuento`, `fecha_pub`, `fecha_iso`, `impresion`, `idioma`, `notas`, `resumen_catalogo`, `resumen_tab_cont`, `ref_bibliografia`, `descriptores`, `materia`, `cat_geografica`, `desc_compra`, `tipo_cambio`, `precio_mn`, `precio_compra_mn`, `precio_compra`, `precio_oferta`, `precio_venta`, `fecha_compra`, `formato`, `medidas`, `stock`, `por_comprar`, `barcode`, `d_responsable`, `img`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba  12332132132132132123', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[]', NULL, '2019-11-20 04:42:22', '2020-01-24 14:11:27'),
	(2, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'BO', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/1-110007.png', NULL, '2019-11-22 22:22:27', '2019-11-22 22:22:27'),
	(3, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'CL', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/1-110007.png', NULL, '2019-11-23 02:57:43', '2019-11-23 02:57:43'),
	(4, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'BO', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/1-110007.png', NULL, '2019-11-23 03:28:50', '2019-11-23 03:28:50'),
	(5, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/3-074301-5.jpg', NULL, '2019-11-26 00:47:22', '2019-11-26 00:47:22'),
	(6, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Nuevo Libro', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-26 01:55:50', '2019-11-26 01:55:50'),
	(7, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-26 02:02:42', '2019-11-26 02:02:42'),
	(8, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-26 02:17:43', '2019-11-26 02:17:43'),
	(9, 1, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'CL', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:32:18', '2019-11-27 19:32:18'),
	(10, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/3-074301-5.jpg', NULL, '2019-11-27 19:38:57', '2019-11-27 19:38:57'),
	(11, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:41:11', '2019-11-27 19:41:11'),
	(12, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/3-074301-5.jpg', NULL, '2019-11-27 19:43:01', '2019-11-27 19:43:01'),
	(13, 1, 'libro antiguo (solo facturar)', NULL, 'Jhon Dick', NULL, 'Título de libro', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:44:49', '2019-11-27 19:44:49'),
	(14, 1, 'libro antiguo (solo facturar)', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:50:25', '2019-11-27 19:50:25'),
	(15, 1, 'libro antiguo (solo facturar)', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:51:37', '2019-11-27 19:51:37'),
	(16, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'CL', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:52:54', '2019-11-27 19:52:54'),
	(17, 1, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:54:53', '2019-11-27 19:54:53'),
	(18, 1, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 19:59:22', '2019-11-27 19:59:22'),
	(19, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 20:07:21', '2019-11-27 20:07:21'),
	(20, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 20:08:25', '2019-11-27 20:08:25'),
	(21, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, '', NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 20:08:43', '2019-11-27 20:08:43'),
	(22, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 20:10:49', '2019-11-27 20:10:49'),
	(23, 1, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 20:12:03', '2019-11-27 20:12:03'),
	(24, 1, 'monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 21:02:39', '2019-11-27 21:02:39'),
	(25, 1, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 21:31:44', '2019-11-27 21:31:44'),
	(26, 1, 'libro antiguo (solo facturar)', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 21:32:30', '2019-11-27 21:32:30'),
	(27, 1, 'donacion revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-27 21:33:51', '2019-11-27 21:33:51'),
	(29, 1, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'CO', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-28 19:59:39', '2019-11-28 19:59:39'),
	(30, 1, 'donacion revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'CO', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-28 20:00:00', '2019-11-28 20:00:00'),
	(31, 1, NULL, NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-11-30 12:38:57', '2019-12-03 19:36:55'),
	(32, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, '2019-11-14', '2019-11-15', NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION PUBLICA\r\n', 'PE', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, '2019-11-16', NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-01 03:09:42', '2019-12-01 03:09:42'),
	(33, 3, 'donacion revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, '2019-11-09', '2019-11-16', NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, 'PE', NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, '2019-11-17', NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-01 03:21:09', '2019-12-01 03:21:09'),
	(34, 3, 'donacion monografia', 'ninguna promo', 'Jhon Dick', 'autor 2', 'Libro de prueba', 'ninguna coleccion', '2', '4', '1234566798', '100', 'Coquito S.A.', 'cocha', 'bolivia', '1ra', 'ningun descuento', '1991-01-31', '1991-10-03', 'epson', 'español', 'ninguna nota', 'ningun resumen', 'ninguna tabla', 'ninguna referencia', 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION', 'EC', 0.00, 1.00, 3.00, 3.00, 40.00, 3.00, 35.00, '1993-10-19', 'carta', '10x10', 1000, 2, '123456789', 'yo ps', 'storage/archivos/14-073218.jpg', NULL, '2019-12-01 03:32:15', '2019-12-03 19:39:34'),
	(35, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 19:25:54', '2019-12-03 21:30:13'),
	(36, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 20:09:48', '2019-12-03 21:29:42'),
	(37, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 20:32:16', '2019-12-03 21:27:35'),
	(38, 2, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 20:51:55', '2019-12-03 22:04:22'),
	(39, 2, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 21:30:54', '2019-12-03 21:32:45'),
	(40, 2, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 21:34:17', '2019-12-03 21:34:17'),
	(41, 2, 'donacion revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 21:40:27', '2019-12-03 21:40:27'),
	(42, 2, 'donacion monografia', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-03 21:42:48', '2019-12-04 03:09:28'),
	(43, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-04 03:16:35', '2019-12-04 03:57:04'),
	(44, 2, 'revista', NULL, 'Jhon Dick', NULL, 'Libro de prueba', NULL, NULL, NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-04 04:04:00', '2019-12-04 04:13:03'),
	(45, 2, 'donacion monografia', 'qweqw', 'Jhon Dick', 'eqwe', 'Libro de prueba', 'qweqwe', 'qwe', NULL, NULL, NULL, 'Coquito S.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', NULL, NULL, NULL, NULL, NULL, NULL, 40.00, NULL, 35.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-04 04:29:48', '2019-12-04 04:30:57'),
	(46, 3, 'revista', NULL, 'Esteban Arce', NULL, 'Libros de prueba', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION', 'CO', 0.00, 1.00, 0.00, 0.00, 40.00, 0.00, 35.00, NULL, NULL, '21 x 14 cm', 1, 1, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-12 16:02:13', '2019-12-12 16:02:13'),
	(47, 3, 'revista', NULL, 'Henrry Mitchell', NULL, 'Titulo de prueba', 'ninguna coleccion', NULL, NULL, 'LA0008732', NULL, 'Coquito', 'Cochabamba', 'Bolivia', '1ra Edicion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION', 'BO', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, '21 x 14 cm', NULL, NULL, 'XOOOT2RQ0R', NULL, 'storage/archivos/3-074301-5.jpg', NULL, '2019-12-12 20:54:50', '2019-12-13 14:16:37'),
	(48, 3, 'revista', NULL, 'Paredes Carmen', NULL, 'Titulo de prueba 13/12', 'ninguna coleccion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION PUBLICA\r\n', 'EC', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, '21 x 14 cm', NULL, NULL, NULL, NULL, 'storage/archivos/14-073218.jpg', NULL, '2019-12-13 19:37:12', '2019-12-13 19:37:12'),
	(49, 3, 'monografia', NULL, 'Stefano Chavez', NULL, 'Estudio de mercadotecnia', 'ninguna coleccion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ARTE - LITERATURA - BIBLIOGRAFIA - BIOGRAFIA - ENSAYO', 'ADMINISTRACION PUBLICA\r\n', 'EC', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, '21 x 14 cm', NULL, NULL, NULL, NULL, 'storage/archivos/1-110007.png', NULL, '2019-12-20 11:00:07', '2019-12-20 11:00:07'),
	(50, 2, 'revista', NULL, 'yo', NULL, '4ewewrwerwerwerwe radfas as fa dasd asd as dad as das dasd asd a das dsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ADMINISTRACION PUBLICA', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, '21 x 14 cm', NULL, NULL, NULL, NULL, 'storage/archivos/5-031933.jfif', NULL, '2020-01-14 03:19:33', '2020-01-14 03:20:37'),
	(51, 1, 'libro antiguo (solo facturar)', '2*1', 'la misma momia', 'momia', 'La momia', 'momia colecctions', '1', NULL, NULL, '265', 'las momias', 'Egipto', 'Egipto', NULL, NULL, '2019-02-03', NULL, NULL, 'español', NULL, NULL, NULL, NULL, NULL, 'ADMINISTRACION', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, '21 x 14 cm', NULL, NULL, NULL, NULL, '[]', NULL, '2020-01-15 20:58:37', '2020-01-24 14:07:41');
/*!40000 ALTER TABLE `libros` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.l_clientes
CREATE TABLE IF NOT EXISTS `l_clientes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `l_clientes_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.l_clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `l_clientes` DISABLE KEYS */;
INSERT INTO `l_clientes` (`id`, `username`, `password`, `email`, `email_verified_at`, `web`, `nombre`, `apellido`, `empresa`, `telefono`, `direccion`, `ciudad`, `pais`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'CLIENTE DE PRUEBA', NULL, 'albany@al.com', NULL, 'albany.com', 'Eduardo', 'Venegas', 'Albany S.A.', '123456789', 'av siempreviva 742 springfield', 'Albany', 'New York', NULL, '2019-12-05 06:08:28', '2019-12-05 06:08:28');
/*!40000 ALTER TABLE `l_clientes` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.l_detalles
CREATE TABLE IF NOT EXISTS `l_detalles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `l_venta_id` bigint(20) unsigned NOT NULL,
  `libro_id` bigint(20) unsigned NOT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editorial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `l_detalles_l_venta_id_foreign` (`l_venta_id`),
  KEY `l_detalles_libro_id_foreign` (`libro_id`),
  CONSTRAINT `l_detalles_l_venta_id_foreign` FOREIGN KEY (`l_venta_id`) REFERENCES `l_ventas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `l_detalles_libro_id_foreign` FOREIGN KEY (`libro_id`) REFERENCES `libros` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.l_detalles: ~35 rows (aproximadamente)
/*!40000 ALTER TABLE `l_detalles` DISABLE KEYS */;
INSERT INTO `l_detalles` (`id`, `l_venta_id`, `libro_id`, `orden`, `autor`, `titulo`, `editorial`, `cantidad`, `precio`, `total`, `unico`, `registro`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(13, 1, 13, '999', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 16:11:20', '2019-12-12 16:11:20'),
	(14, 1, 6, '999', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 16:11:40', '2019-12-12 16:11:40'),
	(16, 3, 6, '312', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 16:18:22', '2019-12-12 16:18:22'),
	(17, 2, 2, '1111', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:23:12', '2019-12-12 17:23:12'),
	(18, 2, 13, '1111', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:28:54', '2019-12-12 17:28:54'),
	(19, 2, 6, '1111', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:29:03', '2019-12-12 17:29:03'),
	(20, 2, 1, '1111', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:32:05', '2019-12-12 17:32:05'),
	(21, 2, 46, '1111', 'Esteban Arce', 'Libros de prueba', NULL, 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:33:05', '2019-12-12 17:33:05'),
	(22, 1, 2, '333', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:35:17', '2019-12-12 17:35:17'),
	(23, 1, 1, '333', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:42:39', '2019-12-12 17:42:39'),
	(24, 1, 46, '333', 'Esteban Arce', 'Libros de prueba', NULL, 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:44:30', '2019-12-12 17:44:30'),
	(25, 1, 6, '333', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:52:32', '2019-12-12 17:52:32'),
	(26, 1, 13, '333', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:53:59', '2019-12-12 17:53:59'),
	(27, 2, 2, '5555', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:55:25', '2019-12-12 17:55:25'),
	(28, 2, 1, '5555', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 17:58:32', '2019-12-12 17:58:32'),
	(29, 2, 46, '5555', 'Esteban Arce', 'Libros de prueba', NULL, 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:01:25', '2019-12-12 18:01:25'),
	(30, 2, 6, '5555', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:05:17', '2019-12-12 18:05:17'),
	(31, 2, 13, '5555', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:06:34', '2019-12-12 18:06:34'),
	(33, 4, 23, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:12:18', '2019-12-12 18:12:18'),
	(34, 2, 19, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:18:55', '2019-12-12 18:18:55'),
	(35, 2, 18, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:19:48', '2019-12-12 18:19:48'),
	(36, 2, 17, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:23:48', '2019-12-12 18:23:48'),
	(37, 4, 16, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:24:27', '2019-12-12 18:24:27'),
	(38, 4, 15, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 18:24:30', '2019-12-12 18:24:30'),
	(39, 5, 47, '987654', 'Henrry Mitchell', 'Titulo de prueba para Libros andinos. Este texto será muy muy largo', 'Coquito', 1, 0.00, 0.00, NULL, NULL, NULL, '2019-12-12 21:04:57', '2019-12-12 21:04:57'),
	(40, 5, 13, '987654', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 21:04:59', '2019-12-12 21:04:59'),
	(41, 5, 6, '987654', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 21:05:02', '2019-12-12 21:05:02'),
	(42, 5, 46, '987654', 'Esteban Arce', 'Libros de prueba', NULL, 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-12 21:05:05', '2019-12-12 21:05:05'),
	(43, 7, 46, '987654', 'Esteban Arce', 'Libros de prueba', NULL, 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-13 19:58:55', '2019-12-13 19:58:55'),
	(44, 7, 6, '987654', 'Jhon Dick', 'Nuevo Libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-13 20:08:57', '2019-12-13 20:08:57'),
	(45, 7, 13, '987654', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2019-12-13 20:08:59', '2019-12-13 20:08:59'),
	(46, 7, 47, '987654', 'Henrry Mitchell', 'Titulo de prueba', 'Coquito', 1, 0.00, 0.00, NULL, NULL, NULL, '2019-12-13 20:09:02', '2019-12-13 20:09:02'),
	(47, 7, 19, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2020-01-14 13:52:41', '2020-01-14 13:52:41'),
	(48, 7, 18, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2020-01-14 13:52:44', '2020-01-14 13:52:44'),
	(49, 7, 16, '9999', 'Jhon Dick', 'Libro de prueba', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2020-01-14 13:52:46', '2020-01-14 13:52:46'),
	(50, 8, 13, '999', 'Jhon Dick', 'Título de libro', 'Coquito S.A.', 1, 35.00, 35.00, NULL, NULL, NULL, '2020-01-24 14:31:42', '2020-01-24 14:31:42');
/*!40000 ALTER TABLE `l_detalles` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.l_ventas
CREATE TABLE IF NOT EXISTS `l_ventas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factura` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `l_cliente_id` bigint(20) unsigned NOT NULL,
  `l_cliente_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aproval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `tbook` decimal(8,2) DEFAULT NULL,
  `porcentaje` decimal(8,2) DEFAULT NULL,
  `discount` decimal(8,2) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `taxes` decimal(8,2) DEFAULT NULL,
  `impuesto` decimal(8,2) DEFAULT NULL,
  `envio` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `glosa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `l_ventas_l_cliente_id_foreign` (`l_cliente_id`),
  CONSTRAINT `l_ventas_l_cliente_id_foreign` FOREIGN KEY (`l_cliente_id`) REFERENCES `l_clientes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.l_ventas: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `l_ventas` DISABLE KEYS */;
INSERT INTO `l_ventas` (`id`, `factura`, `fecha`, `l_cliente_id`, `l_cliente_nombre`, `direccion`, `aproval`, `cantidad`, `tbook`, `porcentaje`, `discount`, `subtotal`, `taxes`, `impuesto`, `envio`, `total`, `glosa`, `head`, `foot`, `mark`, `view`, `user_name`, `unico`, `registro`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, '124', NULL, 1, 'ALBANY', 'Av. Pensilvania #817', 'None', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-10 17:10:51', '2019-12-10 17:10:51'),
	(2, '123', NULL, 1, 'ALBANY', 'Av. Pensilvania #817', 'Andino', 13, 455.00, 0.00, 0.00, 455.00, 0.00, 0.00, 10.00, 465.00, NULL, NULL, 'none', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-10 19:24:40', '2019-12-12 18:23:50'),
	(3, '121', NULL, 1, 'ALBANY', 'Av. Pensilvania #817', 'Bolivia', 0, 0.00, 10.00, 0.00, 0.00, 16.00, 0.00, 10.00, 0.00, NULL, NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-10 19:24:40', '2019-12-12 13:29:14'),
	(4, '555', NULL, 1, 'ALBANY', 'Av. Pensilvania #817 esta es una direccion de prueba larga', 'Bolivia', 7, 245.00, 10.00, 24.50, 220.50, 16.00, 35.28, 5.00, 260.78, NULL, NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-12 18:09:43', '2019-12-12 18:24:32'),
	(5, '987', NULL, 1, 'ALBANY', NULL, 'Bolivia', 10, 210.00, 0.00, 0.00, 210.00, 0.00, 0.00, 0.00, 210.00, NULL, NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-12 21:03:33', '2019-12-12 21:05:07'),
	(6, '666', NULL, 1, 'ALBANY', 'av siempreviva 742 springfield', 'Andino', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, NULL, 'return', 'none', NULL, NULL, NULL, NULL, NULL, '2019-12-13 05:54:58', '2019-12-13 05:54:58'),
	(7, '1234CO2020', NULL, 1, 'CLIENTE DE PRUEBA', 'av siempreviva 742 springfield', 'Bolivia', 18, 525.00, 0.00, 0.00, 525.00, 0.00, 0.00, 0.00, 525.00, NULL, NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2019-12-13 19:56:08', '2020-01-14 13:52:48'),
	(8, '8888888', NULL, 1, 'CLIENTE DE PRUEBA', 'av siempreviva 742 springfield', 'Bolivia', 1, 35.00, 10.00, 3.50, 31.50, 13.00, 4.10, 10.00, 45.59, 'lolo', NULL, 'return', 'pendiente', NULL, NULL, NULL, NULL, NULL, '2020-01-24 14:29:50', '2020-01-24 14:31:45');
/*!40000 ALTER TABLE `l_ventas` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.materias
CREATE TABLE IF NOT EXISTS `materias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grupo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tema` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `pais` int(11) DEFAULT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden_t` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.materias: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `materias` DISABLE KEYS */;
INSERT INTO `materias` (`id`, `descripcion`, `grupo`, `tema`, `orden`, `pais`, `tipo`, `orden_t`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'ADMINISTRACION', 'CIENCIAS SOCIALES', 'POLITICA - ECONOMIA - ECONOMIA POLITICA\r\n', NULL, NULL, NULL, NULL, NULL, '2019-11-22 15:49:48', '2019-11-22 15:49:48'),
	(2, 'ADMINISTRACION PUBLICA\r\n', 'CIENCIAS SOCIALES\r\n', 'POLITICA - ECONOMIA - ECONOMIA POLITICA\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `materias` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.migrations: ~48 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1),
	(9, '2019_10_12_041556_create_catalogos_table', 1),
	(10, '2019_10_15_041640_create_libros_table', 1),
	(11, '2019_10_29_004007_create_l_clientes_table', 1),
	(12, '2019_10_29_004047_create_r_clientes_table', 1),
	(13, '2019_10_29_004136_create_r_ventas_table', 1),
	(14, '2019_10_29_004337_create_revistas_table', 1),
	(15, '2019_10_29_041407_create_materias_table', 1),
	(16, '2019_10_29_041426_create_ofertas_table', 1),
	(17, '2019_10_29_041442_create_pais_table', 1),
	(18, '2019_10_29_041543_create_pedidos_table', 1),
	(19, '2019_10_30_003636_create_l_ventas_table', 1),
	(20, '2019_10_30_003946_create_l_detalles_table', 1),
	(21, '2019_10_30_004110_create_r_detalles_table', 1),
	(22, '2019_10_30_004315_create_numeros_table', 1),
	(23, '2019_11_02_032311_create_suscripcions_table', 1),
	(24, '2019_12_13_171832_create_r_pedidos_table', 1),
	(47, '2014_10_12_000000_create_users_table', 1),
	(48, '2014_10_12_100000_create_password_resets_table', 1),
	(49, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(50, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(51, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(52, '2016_06_01_000004_create_oauth_clients_table', 1),
	(53, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(54, '2019_08_19_000000_create_failed_jobs_table', 1),
	(55, '2019_10_12_041556_create_catalogos_table', 1),
	(56, '2019_10_15_041640_create_libros_table', 1),
	(57, '2019_10_29_004007_create_l_clientes_table', 1),
	(58, '2019_10_29_004047_create_r_clientes_table', 1),
	(59, '2019_10_29_004136_create_r_ventas_table', 1),
	(60, '2019_10_29_004337_create_revistas_table', 1),
	(61, '2019_10_29_041407_create_materias_table', 1),
	(62, '2019_10_29_041426_create_ofertas_table', 1),
	(63, '2019_10_29_041442_create_pais_table', 1),
	(64, '2019_10_29_041543_create_pedidos_table', 1),
	(65, '2019_10_30_003636_create_l_ventas_table', 1),
	(66, '2019_10_30_003946_create_l_detalles_table', 1),
	(67, '2019_10_30_004110_create_r_detalles_table', 1),
	(68, '2019_10_30_004315_create_numeros_table', 1),
	(69, '2019_11_02_032311_create_suscripcions_table', 1),
	(70, '2019_12_13_171832_create_r_pedidos_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.numeros
CREATE TABLE IF NOT EXISTS `numeros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `revista_id` bigint(20) unsigned NOT NULL,
  `num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden_numero` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tomo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `volumen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `numeros_revista_id_foreign` (`revista_id`),
  CONSTRAINT `numeros_revista_id_foreign` FOREIGN KEY (`revista_id`) REFERENCES `revistas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.numeros: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `numeros` DISABLE KEYS */;
INSERT INTO `numeros` (`id`, `revista_id`, `num`, `orden_numero`, `numero`, `tomo`, `volumen`, `fecha`, `descripcion`, `precio`, `registro`, `orden`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 4, '4', '4', '4', '4', '1', NULL, NULL, '20', NULL, '2', NULL, '2020-01-17 15:47:05', NULL),
	(9, 4, '9', '9', '9', '9', '9', '2018-03-01', 'asdasd', '99', '9999', 'si', NULL, '2020-01-17 21:27:46', '2020-01-17 21:28:28');
/*!40000 ALTER TABLE `numeros` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.oauth_access_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.oauth_auth_codes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.oauth_clients: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.oauth_personal_access_clients: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.oauth_refresh_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.ofertas
CREATE TABLE IF NOT EXISTS `ofertas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grupo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `libro_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ofertas_libro_id_foreign` (`libro_id`),
  KEY `ofertas_user_id_foreign` (`user_id`),
  CONSTRAINT `ofertas_libro_id_foreign` FOREIGN KEY (`libro_id`) REFERENCES `libros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ofertas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.ofertas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ofertas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ofertas` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.pais
CREATE TABLE IF NOT EXISTS `pais` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.pais: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.pedidos
CREATE TABLE IF NOT EXISTS `pedidos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `nick` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reclamo` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `l_cliente_id` bigint(20) unsigned NOT NULL,
  `libro_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pedidos_l_cliente_id_foreign` (`l_cliente_id`),
  KEY `pedidos_libro_id_foreign` (`libro_id`),
  CONSTRAINT `pedidos_l_cliente_id_foreign` FOREIGN KEY (`l_cliente_id`) REFERENCES `l_clientes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pedidos_libro_id_foreign` FOREIGN KEY (`libro_id`) REFERENCES `libros` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.pedidos: ~33 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` (`id`, `fecha`, `nick`, `orden`, `estado`, `reclamo`, `cantidad`, `pais`, `unico`, `l_cliente_id`, `libro_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, '2019-12-09', 'ALBANY', '123456', 'vigente', NULL, 1, 'CO', NULL, 1, 13, '2019-12-12 16:14:17', '2019-12-09 19:49:52', '2019-12-12 16:14:17'),
	(2, '2019-12-09', 'ALBANY', '321', 'vigente', NULL, 1, 'CO', NULL, 1, 13, '2019-12-11 16:25:32', '2019-12-09 19:56:40', '2019-12-11 16:25:32'),
	(3, '2019-12-09', 'ALBANY', '321', 'vigente', NULL, 2, 'CO', NULL, 1, 2, '2019-12-12 16:14:41', '2019-12-09 19:57:07', '2019-12-12 16:14:41'),
	(4, '2019-12-09', 'ALBANY', '321', 'vigente', NULL, 2, 'CO', NULL, 1, 2, '2019-12-11 15:35:21', '2019-12-09 20:28:19', '2019-12-11 15:35:21'),
	(5, '2019-12-11', 'ALBANY', '4567', 'vigente', NULL, 1, 'EC', NULL, 1, 4, '2019-12-12 02:46:25', '2019-12-11 15:11:29', '2019-12-12 02:46:25'),
	(7, '2019-12-12', 'ALBANY', '312', 'vigente', NULL, 1, 'BO', NULL, 1, 6, NULL, '2019-12-12 15:35:13', '2019-12-12 16:18:23'),
	(9, '2019-12-12', 'ALBANY', '999', 'agregado', NULL, 1, 'BO', NULL, 1, 13, NULL, '2019-12-12 16:10:20', '2020-01-24 14:31:43'),
	(10, '2019-12-12', 'ALBANY', '999', 'vigente', NULL, 1, 'BO', NULL, 1, 6, NULL, '2019-12-12 16:10:23', '2019-12-12 16:11:42'),
	(11, '2019-12-12', 'ALBANY', '1111', 'vigente', NULL, 1, 'CO', NULL, 1, 13, NULL, '2019-12-12 17:22:53', '2019-12-12 17:28:55'),
	(12, '2019-12-12', 'ALBANY', '1111', 'vigente', NULL, 1, 'CO', NULL, 1, 6, NULL, '2019-12-12 17:22:54', '2019-12-12 17:29:04'),
	(13, '2019-12-12', 'ALBANY', '1111', 'vigente', NULL, 1, 'CO', NULL, 1, 46, NULL, '2019-12-12 17:22:55', '2019-12-12 17:33:06'),
	(14, '2019-12-12', 'ALBANY', '1111', 'vigente', NULL, 1, 'CO', NULL, 1, 1, NULL, '2019-12-12 17:22:56', '2019-12-12 17:32:06'),
	(15, '2019-12-12', 'ALBANY', '1111', 'vigente', NULL, 1, 'CO', NULL, 1, 2, NULL, '2019-12-12 17:22:57', '2019-12-12 17:23:13'),
	(16, '2019-12-12', 'ALBANY', '333', 'vigente', NULL, 1, 'CL', NULL, 1, 13, NULL, '2019-12-12 17:35:01', '2019-12-12 17:54:01'),
	(17, '2019-12-12', 'ALBANY', '333', 'vigente', NULL, 1, 'CL', NULL, 1, 6, NULL, '2019-12-12 17:35:02', '2019-12-12 17:52:33'),
	(18, '2019-12-12', 'ALBANY', '333', 'vigente', NULL, 1, 'CL', NULL, 1, 46, NULL, '2019-12-12 17:35:03', '2019-12-12 17:44:31'),
	(19, '2019-12-12', 'ALBANY', '333', 'vigente', NULL, 1, 'CL', NULL, 1, 1, NULL, '2019-12-12 17:35:04', '2019-12-12 17:42:40'),
	(20, '2019-12-12', 'ALBANY', '333', 'vigente', NULL, 1, 'CL', NULL, 1, 2, NULL, '2019-12-12 17:35:05', '2019-12-12 17:35:18'),
	(21, '2019-12-12', 'ALBANY', '5555', 'vigente', NULL, 1, 'CL', NULL, 1, 13, NULL, '2019-12-12 17:54:56', '2019-12-12 18:06:35'),
	(22, '2019-12-12', 'ALBANY', '5555', 'vigente', NULL, 1, 'CL', NULL, 1, 6, NULL, '2019-12-12 17:54:57', '2019-12-12 18:05:18'),
	(23, '2019-12-12', 'ALBANY', '5555', 'vigente', NULL, 1, 'CL', NULL, 1, 46, NULL, '2019-12-12 17:54:58', '2019-12-12 18:01:26'),
	(24, '2019-12-12', 'ALBANY', '5555', 'vigente', NULL, 1, 'CL', NULL, 1, 1, NULL, '2019-12-12 17:54:59', '2019-12-12 17:58:33'),
	(25, '2019-12-12', 'ALBANY', '5555', 'vigente', NULL, 1, 'CL', NULL, 1, 2, NULL, '2019-12-12 17:55:00', '2019-12-12 17:55:26'),
	(26, '2019-12-12', 'ALBANY', '9999', 'vigente', NULL, 1, 'BO', NULL, 1, 15, NULL, '2019-12-12 18:09:03', '2019-12-12 18:24:31'),
	(27, '2019-12-12', 'ALBANY', '9999', 'agregado', NULL, 1, 'BO', NULL, 1, 16, NULL, '2019-12-12 18:09:04', '2020-01-14 13:52:47'),
	(28, '2019-12-12', 'ALBANY', '9999', 'vigente', NULL, 1, 'BO', NULL, 1, 17, NULL, '2019-12-12 18:09:05', '2019-12-12 18:23:49'),
	(29, '2019-12-12', 'ALBANY', '9999', 'agregado', NULL, 1, 'BO', NULL, 1, 18, NULL, '2019-12-12 18:09:06', '2020-01-14 13:52:45'),
	(30, '2019-12-12', 'ALBANY', '9999', 'agregado', NULL, 1, 'BO', NULL, 1, 19, NULL, '2019-12-12 18:09:07', '2020-01-14 13:52:42'),
	(31, '2019-12-12', 'ALBANY', '9999', 'vigente', NULL, 1, 'BO', NULL, 1, 23, NULL, '2019-12-12 18:09:08', '2019-12-12 18:12:19'),
	(33, '2019-12-12', 'ALBANY', '987654', 'agregado', NULL, 1, 'BO', NULL, 1, 47, NULL, '2019-12-12 21:01:09', '2019-12-13 20:09:02'),
	(34, '2019-12-12', 'ALBANY', '987654', 'agregado', NULL, 1, 'BO', NULL, 1, 13, NULL, '2019-12-12 21:01:18', '2019-12-13 20:09:00'),
	(35, '2019-12-12', 'ALBANY', '987654', 'agregado', NULL, 1, 'BO', NULL, 1, 6, NULL, '2019-12-12 21:01:25', '2019-12-13 20:08:58'),
	(36, '2019-12-12', 'ALBANY', '987654', 'agregado', NULL, 1, 'BO', NULL, 1, 46, NULL, '2019-12-12 21:01:29', '2019-12-13 19:58:56');
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.revistas
CREATE TABLE IF NOT EXISTS `revistas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editorial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isbn_issn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idioma` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formato` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medidas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.revistas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `revistas` DISABLE KEYS */;
INSERT INTO `revistas` (`id`, `titulo`, `editorial`, `isbn_issn`, `ciudad`, `pais`, `idioma`, `formato`, `medidas`, `cantidad`, `registro`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'revista de prueba', 'coquito', '1231654897', 'cocha', 'bolivia', 'español', 'cualquiera', '40x40', '2', 'JoCar', NULL, '2019-11-14 19:46:01', '2019-11-14 19:46:01'),
	(2, 'revista de prueba', 'coquito', '1231654897', 'cocha', 'bolivia', 'español', 'cualquiera', '40x40', '2', 'JoCar', NULL, '2019-11-14 19:47:47', '2019-11-14 19:47:47'),
	(3, 'revista de prueba', 'coquito', '1231654897', 'cocha', 'bolivia', 'español', 'cualquiera', '40x40', '2', 'JoCar', NULL, '2019-11-14 20:22:04', '2019-11-14 20:22:04'),
	(4, 'Condoritosssss', 'condorito', '5454', 'cochabamba', 'Bolivia', 'español', 'revista', NULL, '57', 'si', NULL, '2020-01-16 22:00:09', '2020-01-16 22:09:53');
/*!40000 ALTER TABLE `revistas` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.r_clientes
CREATE TABLE IF NOT EXISTS `r_clientes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `empresa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `r_clientes_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.r_clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `r_clientes` DISABLE KEYS */;
INSERT INTO `r_clientes` (`id`, `username`, `password`, `email`, `email_verified_at`, `web`, `nombre`, `apellido`, `empresa`, `telefono`, `direccion`, `ciudad`, `pais`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'jose', NULL, 'jose@', '2020-01-21 12:02:05', 'jose', 'jose', NULL, NULL, '24234', NULL, NULL, NULL, NULL, '2020-01-21 12:02:36', NULL);
/*!40000 ALTER TABLE `r_clientes` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.r_detalles
CREATE TABLE IF NOT EXISTS `r_detalles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `r_venta_id` bigint(20) unsigned NOT NULL,
  `revista_id` bigint(20) unsigned NOT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editorial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `r_detalles_r_venta_id_foreign` (`r_venta_id`),
  KEY `r_detalles_revista_id_foreign` (`revista_id`),
  CONSTRAINT `r_detalles_r_venta_id_foreign` FOREIGN KEY (`r_venta_id`) REFERENCES `r_ventas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `r_detalles_revista_id_foreign` FOREIGN KEY (`revista_id`) REFERENCES `revistas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.r_detalles: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `r_detalles` DISABLE KEYS */;
/*!40000 ALTER TABLE `r_detalles` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.r_pedidos
CREATE TABLE IF NOT EXISTS `r_pedidos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `nombre_cliente` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reclamo` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `r_cliente_id` bigint(20) unsigned NOT NULL,
  `numero_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `r_pedidos_r_cliente_id_foreign` (`r_cliente_id`),
  KEY `r_pedidos_numero_id_foreign` (`numero_id`),
  CONSTRAINT `r_pedidos_numero_id_foreign` FOREIGN KEY (`numero_id`) REFERENCES `numeros` (`id`) ON DELETE CASCADE,
  CONSTRAINT `r_pedidos_r_cliente_id_foreign` FOREIGN KEY (`r_cliente_id`) REFERENCES `r_clientes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.r_pedidos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `r_pedidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `r_pedidos` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.r_ventas
CREATE TABLE IF NOT EXISTS `r_ventas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `factura` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `r_cliente_id` bigint(20) unsigned NOT NULL,
  `r_cliente_nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aproval` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `tbook` decimal(8,2) DEFAULT NULL,
  `porcentaje` decimal(8,2) DEFAULT NULL,
  `discount` decimal(8,2) DEFAULT NULL,
  `subtotal` decimal(8,2) DEFAULT NULL,
  `taxes` decimal(8,2) DEFAULT NULL,
  `impuesto` decimal(8,2) DEFAULT NULL,
  `envio` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `glosa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `head` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foot` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unico` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `r_ventas_r_cliente_id_foreign` (`r_cliente_id`),
  CONSTRAINT `r_ventas_r_cliente_id_foreign` FOREIGN KEY (`r_cliente_id`) REFERENCES `r_clientes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.r_ventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `r_ventas` DISABLE KEYS */;
INSERT INTO `r_ventas` (`id`, `factura`, `fecha`, `r_cliente_id`, `r_cliente_nombre`, `direccion`, `aproval`, `cantidad`, `tbook`, `porcentaje`, `discount`, `subtotal`, `taxes`, `impuesto`, `envio`, `total`, `glosa`, `head`, `foot`, `mark`, `view`, `user_name`, `unico`, `registro`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'qewqe', '2020-01-21', 1, 'josele', 'adsad', 'adasd', 2, NULL, 3.00, 3.00, 4.00, 5.00, 5.00, 3.00, 5.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-01-21 12:21:42', NULL);
/*!40000 ALTER TABLE `r_ventas` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.suscripcions
CREATE TABLE IF NOT EXISTS `suscripcions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `r_cliente_id` bigint(20) unsigned DEFAULT NULL,
  `revista_id` bigint(20) unsigned NOT NULL,
  `orden` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editorial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registro` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `suscripcions_r_cliente_id_foreign` (`r_cliente_id`),
  KEY `suscripcions_revista_id_foreign` (`revista_id`),
  CONSTRAINT `suscripcions_r_cliente_id_foreign` FOREIGN KEY (`r_cliente_id`) REFERENCES `r_clientes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `suscripcions_revista_id_foreign` FOREIGN KEY (`revista_id`) REFERENCES `revistas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.suscripcions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `suscripcions` DISABLE KEYS */;
INSERT INTO `suscripcions` (`id`, `r_cliente_id`, `revista_id`, `orden`, `titulo`, `editorial`, `estado`, `ciudad`, `pais`, `registro`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 4, 'sss', 'ssss', 'sssss', 'ok', 'cochabamba', 'bolivia', 'si', NULL, '2020-01-17 18:19:48', NULL);
/*!40000 ALTER TABLE `suscripcions` ENABLE KEYS */;

-- Volcando estructura para tabla librosandinos.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empresa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ciudad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pais` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nivel` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla librosandinos.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `email_verified_at`, `web`, `nombre`, `apellido`, `empresa`, `telefono`, `direccion`, `ciudad`, `pais`, `nivel`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(1, 'jocar', '$2y$10$WDxaxHax44Cp3YOAiSr5V.zIFb3Kzy5KUuQfzPHAeCxuzDgUK9BdK', 'aasd@asd.com', NULL, 'asdasdasdasd', 'José Carlos', 'Cuevas', 'Devs', '67454682', 'Quillacollo', 'Cochabamba', 'Bolivia', 1, NULL, NULL, '2019-11-14 21:17:09', '2019-11-14 21:17:09'),
	(2, 'michael', '$2y$10$K2Ah4PCGbVLxixmn8aBr0eGMcpxOLOwUmz5fMtaasDMtndq4rMGu.', 'michael@gmail.com', NULL, 'michael.com', 'micahel', 'mich', 'mich s.a', '13213123', 'qeqwe', 'qweqwe', 'qeqwe', 33, NULL, NULL, '2020-01-21 16:43:07', '2020-01-21 16:43:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
