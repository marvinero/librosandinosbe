<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('l_ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('factura')->nullable();
            $table->date('fecha')->nullable();
            $table->unsignedBigInteger('l_cliente_id')->unsigned();
            $table->foreign('l_cliente_id')
            ->references('id')->on('l_clientes')
            ->onDelete('cascade');
            $table->string('l_cliente_nombre')->nullable();
            $table->string('direccion')->nullable();
            $table->string('aproval')->nullable();
            $table->integer('cantidad')->nullable();
            $table->decimal('tbook')->nullable();
            $table->decimal('porcentaje')->nullable();
            $table->decimal('discount')->nullable();
            $table->decimal('subtotal')->nullable();
            $table->decimal('taxes')->nullable();
            $table->decimal('impuesto')->nullable();
            $table->decimal('envio')->nullable();
            $table->decimal('total')->nullable();
            $table->string('glosa')->nullable();
            $table->string('head')->nullable();
            $table->string('foot')->nullable();
            $table->string('mark')->nullable();
            $table->string('view')->nullable();//revisar
            $table->string('user_name')->nullable();
            $table->string('unico')->nullable();
            $table->string('registro')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('l_ventas');
    }
}
