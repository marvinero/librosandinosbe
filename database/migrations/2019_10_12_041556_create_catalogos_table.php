<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('categoria')->nullable();
            $table->string('tipo')->nullable();
            $table->string('registro')->nullable();
            $table->string('estado')->nullable();
            $table->string('nombre')->nullable();
            $table->string('periodo')->nullable();
            $table->string('gestion')->nullable();
            $table->string('file')->nullable();
            $table->string('pais')->nullable();
            $table->string('send')->nullable();
            $table->string('text')->nullable();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogos');
    }
}
