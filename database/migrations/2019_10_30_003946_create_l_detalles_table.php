<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('l_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('l_venta_id')->unsigned();
            $table->foreign('l_venta_id')
            ->references('id')->on('l_ventas')
            ->onDelete('cascade');
            $table->unsignedBigInteger('libro_id')->unsigned();
            $table->foreign('libro_id')
            ->references('id')->on('libros')
            ->onDelete('cascade');
            $table->string('orden')->nullable();
            $table->string('autor')->nullable();
            $table->string('titulo')->nullable();
            $table->string('editorial')->nullable();
            $table->integer('cantidad')->nullable();
            $table->decimal('precio')->nullable();
            $table->decimal('total')->nullable();
            $table->string('unico')->nullable();
            $table->string('registro')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('l_detalles');
    }
}
