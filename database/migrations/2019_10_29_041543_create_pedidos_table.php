<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->nullable();
            $table->string('nick')->nullable();
            $table->string('orden')->nullable();
            $table->string('estado')->nullable();
            $table->integer('reclamo')->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('pais')->nullable();
            $table->string('unico')->nullable();
            $table->unsignedBigInteger('l_cliente_id')->unsigned();
            $table->foreign('l_cliente_id')
            ->references('id')->on('l_clientes')
            ->onDelete('cascade');
            $table->unsignedBigInteger('libro_id')->unsigned();
            $table->foreign('libro_id')
            ->references('id')->on('libros')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
