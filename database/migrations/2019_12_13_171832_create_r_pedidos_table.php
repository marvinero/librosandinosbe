<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->nullable();
            $table->string('nombre_cliente')->nullable();
            $table->string('orden')->nullable();
            $table->string('estado')->nullable();
            $table->integer('reclamo')->nullable();
            $table->integer('cantidad')->nullable();
            $table->unsignedBigInteger('r_cliente_id')->unsigned();
            $table->foreign('r_cliente_id')
            ->references('id')->on('r_clientes')
            ->onDelete('cascade');
            $table->unsignedBigInteger('numero_id')->unsigned();
            $table->foreign('numero_id')
            ->references('id')->on('numeros')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_pedidos');
    }
}
