<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('catalogo_id')->unsigned();
            $table->foreign('catalogo_id')
            ->references('id')->on('catalogos')
            ->onDelete('cascade');
            $table->string('cod')->nullable();
            $table->string('tipo')->nullable();
            $table->string('promocion')->nullable();
            $table->string('autor')->nullable();
            $table->string('autor_i')->nullable();
            $table->string('titulo');
            $table->string('coleccion')->nullable();
            $table->string('n_volumen')->nullable();
            $table->string('t_volumen')->nullable();
            $table->string('isbn')->nullable();
            $table->string('paginas')->nullable();
            $table->string('editorial')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('pais')->nullable();
            $table->string('edicion')->nullable();
            $table->string('info_descuento')->nullable();
            $table->date('fecha_pub')->nullable();
            $table->date('fecha_iso')->nullable();
            $table->string('impresion')->nullable();
            $table->string('idioma')->nullable();
            $table->string('notas')->nullable();
            $table->string('resumen_catalogo')->nullable();
            $table->string('resumen_tab_cont')->nullable();
            $table->string('ref_bibliografia')->nullable();
            $table->string('descriptores')->nullable();
            $table->string('materia')->nullable();
            $table->string('cat_geografica')->nullable();
            $table->decimal('desc_compra')->nullable();
            $table->decimal('tipo_cambio')->nullable();
            $table->decimal('precio_mn')->nullable();
            $table->decimal('precio_compra_mn')->nullable();
            $table->decimal('precio_compra')->nullable();
            $table->decimal('precio_oferta')->nullable();
            $table->decimal('precio_venta')->nullable();
            $table->date('fecha_compra')->nullable();
            $table->string('formato')->nullable();
            $table->string('medidas')->nullable();
            $table->integer('stock')->nullable();
            $table->integer('por_comprar')->nullable();
            $table->string('barcode')->nullable();
            $table->string('d_responsable')->nullable();
            $table->string('img')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros');
    }
}
