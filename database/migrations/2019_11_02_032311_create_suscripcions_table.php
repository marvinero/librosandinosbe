<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuscripcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suscripcions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('r_cliente_id')->unsigned();
            $table->foreign('r_cliente_id')
            ->references('id')->on('r_clientes')
            ->onDelete('cascade');
            $table->unsignedBigInteger('revista_id')->unsigned();
            $table->foreign('revista_id')
            ->references('id')->on('revistas')
            ->onDelete('cascade');
            $table->string('orden')->nullable();
            $table->string('titulo')->nullable();
            $table->string('editorial')->nullable();
            $table->string('estado')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('pais')->nullable();
            $table->string('registro')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suscripcions');
    }
}
