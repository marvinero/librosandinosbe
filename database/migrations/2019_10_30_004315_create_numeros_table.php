<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNumerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numeros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('revista_id')->unsigned();
            $table->foreign('revista_id')
            ->references('id')->on('revistas')
            ->onDelete('cascade');
            $table->string('num')->nullable();
            $table->string('orden_numero')->nullable();
            $table->string('numero')->nullable();
            $table->string('tomo')->nullable();
            $table->string('volumen')->nullable();
            $table->string('fecha')->nullable();
            $table->string('descripcion')->nullable();
            $table->string('precio')->nullable();
            $table->string('registro')->nullable();
            $table->string('orden')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numeros');
    }
}
