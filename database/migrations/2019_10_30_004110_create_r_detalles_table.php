<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('r_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('r_venta_id')->unsigned();
            $table->foreign('r_venta_id')
            ->references('id')->on('r_ventas')
            ->onDelete('cascade');
            $table->unsignedBigInteger('numero_id')->unsigned();
            $table->foreign('id')
            ->references('id')->on('numeros')
            ->onDelete('cascade');
            $table->string('orden')->nullable();
            $table->string('autor')->nullable();
            $table->string('titulo')->nullable();
            $table->string('editorial')->nullable();
            $table->integer('cantidad')->nullable();
            $table->decimal('precio')->nullable();
            $table->decimal('total')->nullable();
            $table->string('unico')->nullable();
            $table->string('registro')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_detalles');
    }
}
