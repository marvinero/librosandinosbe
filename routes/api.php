<?php

use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use DB;
// use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/generate', function(){


  	$catalogos = DB::select('select * from l_ventas');
    foreach ($catalogos as $key => $value) {
    	DB::update(' libros set catalogo_id = ? where cat = ?',[$value->id,$value->categoria]);
    }

});

Route::get('GenerarCatalogo/{id}', 'GenerarCatalogoController@Wordcito');
Route::get('GenerarCatalogos/{ids}', 'GenerarCatalogoController@Wordcito2');
//Route::get('exportLDetalleExcel/{id}', 'excelController@exportLDetalleExcel');

//Excel
Route::get('GenerarExcelDVLibro/{data}', 'GenerarCatalogoController@excelLibro');

Route::get('GenerarExcelRVLibro/{data}', 'GenerarCatalogoController@excelRevista');

Route::get('/download/{id}', function($id){
  return Excel::download(new UsersExport($id), 'lDetalles.xlsx');
});



Route::get('tasks', 'TaskController@index');
Route::get('tasks/{id}', 'TaskController@show');
Route::post('tasks', 'TaskController@store');
Route::put('tasks/{id}', 'TaskController@update');
Route::delete('tasks/{id}', 'TaskController@delete');
Route::post('register', 'UserController@register');
Route::get('profile', 'UserController@getAuthenticatedUser');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('revistas', 'RevistaController');
Route::resource('catalogos', 'CatalogoController');
Route::get('catalogosDataLibro', 'CatalogoController@catalogoLibro');
Route::resource('l_clientes', 'LClientesController');
Route::resource('l_detalles', 'LDetallesController');
Route::resource('l_ventas', 'LVentaController');
Route::resource('libros', 'LibroController');
Route::resource('l_venta', 'LVentaController');
Route::resource('materias', 'MateriaController');
Route::resource('numeros', 'NumeroController');
Route::resource('pais', 'PaisController');
Route::resource('pedidos', 'PedidoController');
Route::resource('r_pedidos', 'RPedidosController');
Route::resource('r_clientes', 'RClientesController');
Route::resource('r_detalles', 'RDetallesController');
Route::resource('r_ventas', 'RVentaController');
Route::resource('usuario', 'UsuarioController');
Route::resource('users', 'UserController');
Route::resource('suscripcions', 'SuscripcionController');
Route::resource('consultar', 'ConsultarController');
Route::put('libros', 'LibroController@update');
Route::put('materias', 'MateriaController@update');

Route::group(['middleware' => ['jwt.auth']], function(){
	Route::get('user', 'TokensController@user');
	Route::get('/auth/expire', 'TokensController@expireToken');
});

Route::group(['middleware' => []], function(){
	Route::post('login', 'TokensController@login');
	Route::post('/auth/refresh', 'TokensController@refreshToken');
	Route::get('/auth/logout', 'TokensController@logout');
});
